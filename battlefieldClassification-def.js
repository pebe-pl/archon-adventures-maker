var armaments=[
	{id:"hands",    Force:2,  Speed:3,    Interval:0.25,  Duration:10, type:"weapon", mode:"attack",   name:"Hands"},
	{id:"bow",      Force:5,  Speed:2,    Interval:0.40,  Duration:15, type:"weapon", mode:"attack",   name:"Bow",         tile:"weapon_bow"},
	{id:"arrows",   Force:1,  Speed:4,    Interval:0.40,  Duration:15, type:"weapon", mode:"ammo",     name:"Arrows",      tile:"weapon_arrows"},
	{id:"spear",    Force:6,  Speed:3,    Interval:0.80,  Duration:30, type:"weapon", mode:"attack",   name:"Spear",       tile:"weapon_spear"},
	{id:"sword",    Force:5,  Speed:3,    Interval:0.40,  Duration:15, type:"weapon", mode:"attack",   name:"Sword",       tile:"weapon_sword"},
	{id:"axe",      Force:5,  Speed:2,    Interval:0.40,  Duration:15, type:"weapon", mode:"attack",   name:"Axe",         tile:"weapon_axe"},
    {id:"shield",   Force:1,  Speed:3,    Interval:1,     Duration:50, type:"weapon", mode:"defense",  name:"Shield Small",tile:"weapon_shieldsmall"},
    {id:"shieldbig",Force:3,  Speed:2,    Interval:1,     Duration:50, type:"weapon", mode:"defense",  name:"Shield Big",  tile:"weapon_shieldbig"}
]

var spells=[
	{id:"energy_bolt"  ,Force:7,  Speed:7,    Interval:0.60,  Duration:30, type:"spell",  mode:"attack",   name:"Energy Bolt"},
	{id:"fireball"     ,Force:10, Speed:5,    Interval:0.80,  Duration:30, type:"spell",  mode:"attack",   name:"Fireball"},
	{id:"whirlwind"    ,Force:6,  Speed:5,    Interval:0.90,  Duration:20, type:"spell",  mode:"attack",   name:"Whirlwind"},
	{id:"eye_beam"     ,Force:9,  Speed:7,    Interval:0.60,  Duration:50, type:"spell",  mode:"attack",   name:"Eye Beam"},
	{id:"lighting_bolt",Force:8,  Speed:6,    Interval:0.80,  Duration:30, type:"spell",  mode:"attack",   name:"Lighting Bolt"},
	{id:"tile_spikes"  ,Force:4,  Speed:3,    Interval:0.80,  Duration:40, type:"spell",  mode:"attack",   name:"Tile Spikes"},
	{id:"boulder"      ,Force:10, Speed:3,    Interval:1.00,  Duration:20, type:"spell",  mode:"attack",   name:"Boulder"},
	{id:"fiery_breath" ,Force:11, Speed:4,    Interval:1.20,  Duration:50, type:"spell",  mode:"attack",   name:"Fiery Breath"},
	{id:"scream"       ,Force:8,  Speed:3,    Interval:1.00,  Duration:40, type:"spell",  mode:"attack",   name:"Screem"},
]

var armamentStat={
    'q1':        (stat) => { return (stat.Duration/50)/stat.Interval; },
    'q2':        (stat) => { return 1/((stat.Duration/50)/stat.Interval); },
    'damage':    (stat) => {
        if (stat.mode=="attack" || stat.mode=="ammo")
            var v=(stat.Force*stat.Speed)*stat.q2
        else
            v=0;
        return v
    },
    'defense':   (stat) => { 
        if (stat.mode=="defense")
            v=stat.Force*stat.Speed*stat.q2;
        else
            v=0;
        return v
    },
    'dexterity': (stat) => {
        v=(stat.Speed*stat.q2)*stat.q1
        return v; }
}

var monsters=[
    {name:"Unicorn",       Side:"Light", Lifespan:9,  Armament:{spell:"energy_bolt"},             Range:4,  Speed:1.00},
    {name:"Wizard",        Side:"Light", Lifespan:10, Armament:{spell:"fireball"},                Range:3,  Speed:1.00},
    {name:"Archer",        Side:"Light", Lifespan:5,  Armament:{attack:"bow",ammo:'arrows'},      Range:3,  Speed:1.00},
    {name:"Golem",         Side:"Light", Lifespan:15, Armament:{spell:"boulder"},                 Range:3,  Speed:0.75},
    {name:"Valkyrie",      Side:"Light", Lifespan:8,  Armament:{attack:"spear"},                  Range:3,  Speed:1.00},
    {name:"Djinni",        Side:"Light", Lifespan:15, Armament:{spell:"whirlwind"},               Range:4,  Speed:1.00},
    {name:"Phoenix",       Side:"Light", Lifespan:12, Armament:{spell:"fiery_breath"},            Range:5,  Speed:1.00},
    {name:"Knight",        Side:"Light", Lifespan:5,  Armament:{defense:'shield',attack:"sword"}, Range:3,  Speed:1.00},

	{name:"Basilisk",      Side:"Dark", Lifespan:6,   Armament:{spell:"eye_beam"},	              Range:3,	Speed:1.00, tile:"monster_basilisk"},
	{name:"Sorceress",     Side:"Dark", Lifespan:15,  Armament:{spell:"lighting_bolt"},           Range:3,	Speed:1.00, tile:"monster_sorceress"},
	{name:"Manticore",     Side:"Dark", Lifespan:8,   Armament:{spell:"tile_spikes"},             Range:3,	Speed:1.00, tile:"monster_manticore"},
	{name:"Troll",         Side:"Dark", Lifespan:14,  Armament:{spell:"boulder"},                 Range:3,	Speed:0.75, tile:"monster_troll"},
	{name:"Shapeshifter",  Side:"Dark", Lifespan:10,  Armament:{attack:"hands"},                  Range:5,	Speed:0.75, tile:"monster_shapeshifter"},
	{name:"Saurian",       Side:"Dark", Lifespan:17,  Armament:{spell:"fiery_breath"},            Range:4,	Speed:1.00, tile:"monster_saurian"},
	{name:"Banshee",       Side:"Dark", Lifespan:8,   Armament:{spell:"scream"},		          Range:3,	Speed:1.00, tile:"monster_banshee"},
	{name:"Goblin",        Side:"Dark", Lifespan:5,   Armament:{attack:"axe"},            	      Range:3,	Speed:1.00, tile:"monster_goblin"}
]

function getArmamentStat(name){
    name=name.trim().toLowerCase();
    for (armamentID in armaments) {
        var armament=armaments[armamentID]
        if (armament.id==name) return armament;
    }
    for (spellID in spells) {
        var spell=spells[spellID]
        if (spell.id==name) return spell;
    }
    return null;
}
var monsterStat={
	'damage':    (stat) => {
	    var v=stat.Range*stat.Speed
        for (arm in stat.Armament) {
            armName=stat.Armament[arm];
            var armV=getArmamentStat(armName);
            if ( armV!=null ) v+=armV.damage
        }
        return v;
	},
	'health':    (stat) => {
	    var v=stat.Lifespan*stat.Range*stat.Speed
	    return v;
	},
	'dexterity': (stat) => { 
        var v=(stat.Range*stat.Lifespan*(stat.Speed))
        for (arm in stat.Armament) {
            armName=stat.Armament[arm];
            var armV=getArmamentStat(armName);
            if ( armV!=null ) v+=armV.dexterity
        }
        return v;
	},
	'defense':   (stat) => { 
	   var v=(stat.Lifespan/stat.Range);
	   for (arm in stat.Armament) {
	       armName=stat.Armament[arm];
           var armV=getArmamentStat(armName);
           if ( armV!=null ) v+=armV.defense
	   }
	   return v;
	}
}

var statGeneralCalculate={
	'maxDamage': (generalStat) => { return parseInt(generalStat.dexterity/(generalStat.dexterity+generalStat.health)*100); },
	'fightBack': (generalStat) => { return parseInt(Math.sqrt(Math.sqrt(generalStat.defense*generalStat.health)*generalStat.dexterity)); },
	'rank':      (generalStat) => { return parseInt((Math.sqrt(generalStat.health*generalStat.dexterity)+Math.sqrt((generalStat.defense*(generalStat.general.fightBack/100))*(generalStat.damage*(generalStat.general.maxDamage/100)))/2)); }
};
