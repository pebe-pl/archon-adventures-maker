var overlay,		// overlay div for menu & context-menu
	overlay_menu;	// object menu or contextmenu

var	clipboard = {
		"width":0,
		"height":0,
		"data":[]
	};

var selectStartX,selectStartY,
	selectEndX,selectEndY,
	selectWidth,selectHeight,
	selectCancel=false;

//
// View - Grid Visibility
//

function setGrid() {
	userConfig['screenGrid']=$('#view-ScreenGrid').prop('checked');
	userConfig['fontSetGrid']=$('#view-FontSetGrid').prop('checked');
	grid.clearRect(0,0,mapWidth*charWidth,mapHeight*charHeight);
	drawScreenGrid();
	drawFontSetGrid();
}

//
//  Drawing - Mode
//

function setMouseMode(res) {
	drawMode=res;
	if (res == 'paint') {
		$('#dlg-Tiles div.tiles.select').removeClass('select');
		if (currentTileGroup=="tile") {
			cursorWidth=tiles[currentTile].width;
			cursorHeight=tiles[currentTile].height;
			$('#tile_'+currentTile).addClass('select');
		} else {
			cursorWidth=1;
			cursorHeight=1;
		}
	}
	if (res == 'select') {
		selectCancel=true;
		cursorHeight=1;
		cursorWidth=1;
	}
	if (res == 'object') {
		selectCancel=true;
		cursorHeight=1;
		cursorWidth=1;
	}
	if (grid!==undefined) {
		grid.clearRect(0,0,mapWidth*charWidth,mapHeight*charHeight);
		drawScreenGrid();
	}
}

//
//
//

function setPaintMethod(res) {
	paintMethod=res;
	$('#menu-content #tabDrawing input').prop('checked',false);
	$('#menu-content #tabDrawing input#'+drawMode+'-'+paintMethod).prop('checked',true);
	$('#menu-content #tabDrawing input').checkboxradio('refresh');
	if (drawMode=='select') {
	}
	if (drawMode=='paint') {
		if (paintMethod=="line" || paintMethod=="box") {
			cursorWidth=1;
			cursorHeight=1;
		}
	}
}

//
// Selection action Clear/Copy/Paste/MakeTile
//

function doSelectClear() {
	for (y=selectStartY;y<=selectEndY;y++) {
		for (x=selectStartX;x<=selectEndX;x++) {
			adr=x+y*mapWidth;
			dataMap[adr]=0;
			drawChar(x,y,0,map);
		}
	}
	clipboard['width']=0;
	clipboard['height']=0;
	clipboard['data']=[];
}

function selectionClear() {
	if (userConfig['confirmSelectionClear']) {
		var dlgConfirm=$('<div id="messageBox" title="Confirm"/>').html(' \
		<div class="text-center"> \
			<h2>The action is irreversible.</h2> \
			<p>Are you sure you want to clear the selected area?</p> \
			<input id="confirmSelectionClear" type="checkbox"><label \ for="confirmSelectionClear">Never show this message</label> \
		</div> \
		').dialog({
			width: 400,
			autoopen:true,
			resizable:false,
			modal:true,
			buttons: {
				"Ok, don't do this": function() { $(this).dialog('close'); },
				"Do it": function() { doSelectClear(); $(this).dialog('close'); },
			},
			create: function() {
				$('#confirmSelectionClear').checkboxradio();
			},
			close: function () {
				userConfig['confirmSelectionClear']=!$('#confirmSelectionClear').prop('checked');
				saveConfig();
				$('#confirmSelectionClear').checkboxradio("destroy");
				$(this).dialog('destroy');
			}
		});
	} else {
		doSelectClear();
	}
}

function doSelectionCopy() {
	var i=0;
	clipboard['width']=selectWidth;
	clipboard['height']=selectHeight;
	clipboard['data']=[];
	for (y=selectStartY;y<=selectEndY;y++) {
		for (x=selectStartX;x<=selectEndX;x++) {
			adr=x+y*mapWidth;
			clipboard[i]=dataMap[adr];
			i++;
		}
	}
}

function selectionCopy() {
	doSelectionCopy();

	if ( userConfig['infoSelectionCopy'] ) {
		var dlgInfo=$('<div id="messageBox" title="Information"/>').html(' \
		<div class="text-center"> \
			<p>Area has been copied to the clipboard.</p> \
			<input id="infoSelectionCopy" type="checkbox"><label \ for="infoSelectionCopy">Never show this message</label> \
		</div> \
		').dialog({
			width: 400,
			autoopen:true,
			resizable:false,
			modal:true,
			buttons: {
				"I understand": function() { $(this).dialog('close'); },
			},
			create: function() {
				$('#infoSelectionCopy').checkboxradio();
			},
			close: function () {
				userConfig['infoSelectionCopy']=!$('#infoSelectionCopy').prop('checked');
				saveConfig();
				$('#infoSelectionCopy').checkboxradio("destroy");
				$(this).dialog('destroy');
			}
		});
	}
}

function doClipboardPaste(x,y) {
	var pasteWidth=clipboard['width']-1,
		pasteHeight=clipboard['height']-1,
		pasteStartX=x,
		pasteStartY=y,
		pasteEndX=x+pasteWidth,
		pasteEndY=y+pasteHeight;

	var i=0;
	for (y=pasteStartY;y<=pasteEndY;y++) {
		for (x=pasteStartX;x<=pasteEndX;x++) {
			adr=x+y*mapWidth;
			dataMap[adr]=clipboard[i];
			drawChar(x,y,clipboard[i],map);
			i++;
		}
	}
}

function clipboardPaste() {
	var pasteWidth=clipboard['width'],
		pasteHeight=clipboard['height'];

	if (pasteWidth>0 && pasteHeight>0) {
		setMouseMode('paste');
		setPaintMethod('paste');
		cursorWidth=clipboard['width'];
		cursorHeight=clipboard['height'];
		grid.clearRect(0,0,2048,2048);
		drawScreenGrid();
		return;
	}

	if ( userConfig['infoClipboardPaste'] ) {
		var dlgInfo=$('<div id="messageBox" title="Information"/>').html(' \
		<div class="text-center"> \
			<h2>The clipboard is empty.</h2> \
			<p>Befor pasteing, copy first.</p> \
			<input id="infoClipboardPaste" type="checkbox"><label \ for="infoClipboardPaste">Never show this message</label> \
		</div> \
		').dialog({
			width: 400,
			autoopen:true,
			resizable:false,
			modal:true,
			buttons: {
				"I understand": function() { $(this).dialog('close'); },
			},
			create: function() {
				$('#infoClipboardPaste').checkboxradio();
			},
			close: function () {
				userConfig['infoClipboardPaste']=!$('#infoClipboardPaste').prop('checked');
				saveConfig();
				$('#infoClipboardPaste').checkboxradio("destroy");
				$(this).dialog('destroy');
			}
		});
	}

}

//
// issue #48
//

/*
function doMakeTile() {
	var newTileName=$('#newTileName').val().trim();

	if (newTileName=="") {
		understandDialog("Information", "Name can not be empty", "", 300);
		return false;
	}

	if ( tiles[newTileName]===undefined ) {
		var entry=newTileName, tileData=[], i=0;

		for (y=0;y<selectHeight;y++) {
			for (x=0;x<selectWidth;x++) {
				adr=(selectStartX+x)+(selectStartY+y)*mapWidth;
				tileData.push(dataMap[adr]);
				i++;
			}
		}

		tiles[newTileName]={"group":"user","width":selectWidth,"height":selectHeight,"data":tileData};
		makeTile("user",newTileName,selectWidth,selectHeight,tileData);

		$('#dlg-Tiles select#tiles-group-select option[value="user"]').prop('selected',true);
		$('#dlg-Tiles select#tiles-group-select').selectmenu('refresh');
		$('#dlg-Tiles div.tiles-group').addClass('hidden');
		$('#dlg-Tiles div#tiles-group--user').removeClass('hidden');
		var $c=$('#dlg-Tiles');
		$c.stop().scrollTo('a#tile_'+newTileName,{duration:500});

		return true;
	} else {
		understandDialog("Information", "The tile name already exists.", "Despite the grouping, the tile names cannot be the same in different groups.", 500);
	}
}

function selectionMakeTile() {
	if (selectWidth>screenWidth || selectHeight>screenHeight) {
		understandDialog("Information", "Selection is too big.", "The size of the created tile cannot be larger than the size of the screen grid.", 400);
		return false;
	}

	$('<div id="dialog-makeTile" title="Make tile from selection"/>')
	.html('<fieldset class="block text-center"><legend>Tile:</legend> \
		   <div>Tile size: '+selectWidth+' x '+selectHeight+'</div> \
		   <div class="preview-tile"></div> \
		   </fieldset> \
		   <label for="newTitleName" class="block">New tile name</label> \
		   <input id="newTileName" type="text" value="" class="ui-inputline ui-widget ui-corner-all full-width"> \
	')
	.dialog({
		width: 570,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Don't do this": function() { $(this).dialog('close'); },
			"Make": function() { 
				if ( doMakeTile() ) { $(this).dialog('close'); } }
		},
		create: function() {
			var userTileCanvas=$('<canvas id="previewUserTilePreviewCanvas" width="'+(selectWidth*16)+'" height="'+(selectHeight*32)+'"/>');
			var userTile=userTileCanvas[0].getContext('2d');

			var i=0;
			for (y=0;y<selectHeight;y++) {
				for (x=0;x<selectWidth;x++) {
					adr=(selectStartX+x)+(selectStartY+y)*mapWidth;
					drawChar(x,y,dataMap[adr],userTile);
					i++;
				}
			}
			$('#dialog-makeTile div.preview-tile').append(userTileCanvas);
			userTileCanvas.on('click',selectTile);
			userTileCanvas.on('contextmenu',showContextMenuTile);

		},
		close: function () {
			$(this).dialog('destroy');
		}
	});
}
*/

//
// Initialize Menubar
//

function initMenu() {
	$('#menu-content').tabs();

	$('#menu-content #tabDrawing input').checkboxradio({icon:false});
	$('#menu-content #tabView .grid.group').controlgroup();
}
