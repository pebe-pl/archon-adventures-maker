var userConfig = {
    'version': "v0.604",
    'confirmSelectionClear': true,
    'infoSelectionCopy': true,
    'infoClipboardPaste': true,
    'infoNewWorld': true,
    'infoClearShortcut': true,
    'info_characterSetOnCreateOnly': true,
    'info_immediatelyRemberedOperation': true,
    'firstRun': true,
    'screenGrid': true,
    'fontSetGrid': true,
    'tool_window':{
    },
    'user': {
        "nick": "",
        "country": "",
        "contact": ""
    }
};

//
//
//

function saveConfig() {
    Cookies.set('AAM', JSON.stringify(userConfig));
    console.log('Configuration are saved in cookie.');
}

function loadConfig() {
    var o = Cookies.get('AAM');
    if (o !== undefined) {
        newConfig = JSON.parse(o);
        for (entry in newConfig) {
            if (entry == "version")
                continue;
            userConfig[entry] = newConfig[entry];
        }
        console.log('Configuration are loaded from cookie.');
    } else {
        console.log('Not found cookie configuration!');
    }
}
//
//
//

function understandDialog(title, main, desc, dlg_width) {
    var dlgInfo = $('<div id="messageBox" title="' + title + '"/>').html('<div class="text-center"><h2>' + main + '</h2><p>' + desc + '</p></div>').dialog({
        width: dlg_width,
        autoopen: true,
        resizable: false,
        modal: true,
        buttons: {
            "I understand": function() {
                $(this).dialog('close');
            },
        },
        close: function() {
            $(this).dialog('destroy');
        }
    });

}

//
// Initialize
//

function aboutApp() {
    overlay = $('<div class="menu-overlay about"/>').click(function() {
        overlay.remove();
        $('#AAM_APP div#title').removeClass('about');
        $('#AAM_APP #title').on('click', aboutApp);
    }).appendTo('body');
    $('#AAM_APP div#title').addClass('about');
    $('#AAM_APP #title').off('click', aboutApp);
}

//
//
//
function getWindowConfigProperty(ui,prop) {
    let dlg=$(ui);
    if ( dlg.length!==0 ) {
        let dlgID=dlg.prop('id');
        if (userConfig['tool_window'][dlgID]!==undefined)
            if (userConfig['tool_window'][dlgID][prop]!==undefined)
                return userConfig['tool_window'][dlgID][prop];
    }
    return false;
}


function getWindowConfig(ui,defaults) {
    let dlg=$(ui);
    if ( dlg.length!=0 ) {
        dlgID=dlg.prop('id');
        dlgParent=dlg.parent();
        if ( userConfig['tool_window'][dlgID]===undefined ) {
    		console.log('Window "'+dlg.prop('id')+'" has no saved configuration.');
    		if ( defaults!==undefined ) {
                console.log('Apply default settings.',defaults)
                return defaults;
    		}
    		console.warn('Default parameters are not passed!');
    		return false;
        }
   		console.log('Apply stored settings to window "'+dlg.prop('id')+'".',userConfig['tool_window'][dlgID]);
        return userConfig['tool_window'][dlgID];
    } else {
        console.error('checkWindowConfig attempts to reference a non-existing resource!')
    }
    return defaults;
}

function saveWindowConfig(ui) {
    let dlg=$(ui);
    if ( dlg.length!=0 ) {
        dlgID=dlg.prop('id');
        dlgParent=dlg.parent();
        if ( userConfig['tool_window'][dlgID]===undefined )
            userConfig['tool_window'][dlgID]={};

        userConfig['tool_window'][dlgID].visible=dlg.dialog('isOpen')
        let top=parseInt(dlgParent.css('top')),
            left=parseInt(dlgParent.css('left')),
            dockID=$(checkDockAttach(dlg)).prop('id');

        if ( dockID===undefined ) dockID=false;

        userConfig['tool_window'][dlgID].position={my:"left top", at:`left+${left} top+${top}`, of: "#viewport"}
        userConfig['tool_window'][dlgID].top=top
        userConfig['tool_window'][dlgID].left=left
        userConfig['tool_window'][dlgID].width=parseInt(dlgParent.css('width'))
        userConfig['tool_window'][dlgID].height=parseInt(dlgParent.css('height'))
        userConfig['tool_window'][dlgID].dockPanel=dockID;
        saveConfig(); 
    } else {
        console.error('saveWindowConfig attempts to reference a non-existing resource!')
    }
}

//
//
//

function checkDockAttach(dlg) {
    let dlgID = dlg.prop('id');

    // check dock panel
    dockList = $("#viewport div.dockPanel").find("div");
    let attached;

    dockList.each(function(id, el) {
        let dockDlgID = $(el).data('dlgid');
        if (dockDlgID == dlgID) {
            attached=$(el).parent();
        }
    })

    return attached;
}

function undockDialog(dlg, docker) {
    var panel;
    if ( typeof docker===`object` ) {
        panel = docker.prop('id').slice(5);
    } else if ( typeof docker==="string" ) {
        panel=docker.slice(5);
        docker=$('#'+docker);
    } else {
        console.error('Invalid dialog undocking call parameter');
        return false;
    }

    var dlgID = dlg.prop('id');

    // check dock panel
    dockList = $(docker).find("div");
    var removed=false
        , panelWidth=0;

    dockList.each(function(id, el) {
        var dockDlgID = $(el).data('dlgid');
        var dlgWidth=$('div#'+dockDlgID).parent().width();
        if (dockDlgID == dlgID) {
            $(el).remove();
            removed=true
        } else {
            if ( dlgWidth>panelWidth ) panelWidth=dlgWidth+2;
        }
    })
    $("body").get(0).style.setProperty("--" + panel + "PanelWidth", panelWidth+"px")
}

function dockDialog(dlg, docker) {
    var panel;
    if ( typeof docker===`object` ) {
        panel = docker.prop('id').slice(5);
    } else if ( typeof docker==="string" ) {
        panel=docker.slice(5);
        docker=$('#'+docker);
    } else {
        console.error('Invalid dialog docking call parameter');
        return false;
    }

    var dlgID = dlg.prop('id');

    dlgMain = dlg.parent();
    var dlgWidth = dlgMain.width()
      , dlgHeight = dlgMain.height()
      , panelWidth = $("div#dock-" + panel + ".dockPanel").width();

    if (panelWidth < dlgWidth)
        $("body").get(0).style.setProperty("--" + panel + "PanelWidth", (dlgWidth + 2) + "px")

    dockList = $(docker).find("div");
    var found = false;
    dockList.each(function(id, el) {
        var dockDlgID = $(el).data('dlgid');
        if (dockDlgID == dlgID) {
            found = true;
        }
    })

    if (!found) {
        var dockItem = $('<div data-dlgid="' + dlgID + '"/>');
        docker.append(dockItem);
    }
    return true;
}

//
//
//

var interval = setInterval(function() {
    if (document.readyState === 'complete') {
        clearInterval(interval);
        Init();
    }
    let ohtml=$('#LOADER_APP #loader').html();
    $('#LOADER_APP #loader').html(ohtml+'.');
}, 100);

function Init() {
    loadConfig();
    $('body').disableSelection();
    initCharacterMapDialog();
    initTilesDialog();
    initLocationsDialog();
    initMapEditor();
    initMenu();
    initObjectLocation();
    initGeneralClassification();
    //
    // Show application
    $('#LOADER_APP').fadeOut(1000, ()=>{
        $('#AAM_APP').removeClass('hidden');
        $('#AAM_APP #title').on('click', aboutApp);
        if (userConfig['firstRun']) {
            userConfig['firstRun'] = false;
            aboutApp();
            saveConfig();
        }
        var docker = $("div.dockPanel")
        docker.droppable({
            tolerance: 'touch',
            drop: function(e, u) {
                var dlg = u.draggable.find('.dockable')
                if (dlg.length == 0)
                    return false;
                var docker = $(this);
                console.log('Drop dialog "'+dlg.prop('id')+'" in panel "'+docker.prop('id')+'"')
                dockDialog(dlg, docker)
            },
            out: function(e, u) {
                var dlg = u.draggable.find('.dockable')
                if (dlg.length == 0)
                    return false;
                var docker = $(this);
                console.log('Drop out dialog "'+dlg.prop('id')+'" from panel "'+docker.prop('id')+'"')
                var panel = docker.prop('id').slice(5);
                undockDialog(dlg, docker);
            }
        });

        let toolDialogs={
            'dlg-mapCharacters':{'show':showCharactersMapDialog},
            'dlg-Tiles':{'show':showTilesDialog},
            'dlg-locations':{'show':showLocationDialog},
            'dlg-objectLocation':{'show':showObjectLocationDialog}
        };

        for ( let dlgID in toolDialogs ) {
            let dlg=$('#'+dlgID),
                ui=$(dlg).closest('.ui-dialog');

            ui.draggable('option','containment','#viewport');
            ui.draggable('option','snap','#viewport');

            if ( getWindowConfigProperty(dlg,'visible') ) {
                toolDialogs[dlgID].show();
            }
        }
    });
}
