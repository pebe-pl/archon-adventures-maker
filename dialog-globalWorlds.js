function dialogGlobalWorldsService() {
	var id="dlg-globalWorlds", dlgId="#"+id+" ";
	
	$('<div id="'+id+'" title="Global Worlds Service" />').html('<ul id="data-header"><li><span class="field filename">Name</span><span class="field locationsCount">Locations</span><span class="field userNick">Author</span><span class="field userCountry">Country</span><span class="field version">Ver</span></li></ul><div id="data"></div>')
	.dialog({
		autoopen:true,
		width:800,
		height:450,
		resizable:false,
		modal:true,
		buttons:{
			'Leave service':function() { $(this).dialog('close'); },
		},
		create:function(){
			$(dlgId+"#data").html('<div class="text-center">Loading Worlds list...</div>');
			$.get('global-list.php')
			.done(function(data) {
				$(dlgId+"#data").html('<ul id="gws-list">'+data+'</ul>');
			});
		},
		close:function(){
			$(this).dialog('destroy');
		}
	});
}