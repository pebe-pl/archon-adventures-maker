var currentFile="archon-adventures",
	currentFileFormat="JSON";

function doCreateNewWorld() {
	currentChars=-1;
	changeTilesSet(currentChars);
	locationList=[];
	objectsList=[];
	currentLocation=0;
	$('#map-content').empty();
	$('#dlg-locations ul#list-locations').empty();
	$('div#dlg-objectLocation ul#list').empty()
	createLocationDialog("Empty location",64,16);
	$('div[id^=object-edit-').dialog('close');
}

function createNewWorld() {
	if ( userConfig['infoNewWorld'] && map!==undefined) {
		var dlgInfo=$('<div id="dlg-loadFile" title="Attention"/>').html(' \
		<div class="text-center"> \
			<h2>All data will be deleted</h2> \
			<input id="infoNewWorld" type="checkbox"><label \ for="infoNewWorld">Never show this message</label> \
		</div> \
		').dialog({
			width: 440,
			autoopen:true,
			resizable:false,
			modal:true,
			buttons: {
				"I understand": function() {
					doCreateNewWorld();
					$(this).dialog('close');
				},
				"F**k, go back!": function() { $(this).dialog('close'); },
			},
			create: function() {
				$('#infoNewWorld').checkboxradio();
			},
			close: function () {
				userConfig['infoNewWorld']=!$('#infoNewWorld').prop('checked');
				saveConfig();
				$('#infoNewWorld').checkboxradio("destroy");
				$(this).dialog('destroy');
			}
		});
	} else {
		doCreateNewWorld();
	}
}

//
//
//

function readBlob(files) {
	if (!files.length) {
		return;
	}
   	$('div[id^="object-edit-"').dialog('close');

	var dlgInfo=$('<div id="messageBox" title=""/>').html(' \
	<div class="text-center"> \
		<h2>Loading data...</h2> \
	</div> \
	').dialog({
		width: 400,
		autoopen:true,
		resizable:false,
		modal:true,
		close: function () {
			$(this).dialog('destroy');
		}
	});

	var file=files[0],
		start=0, stop=file.size - 1;

	currentFile=file.name;

	var reader = new FileReader();
	// If we use onloadend, we need to check the readyState.
	reader.onloadend = function(evt) {
		if (evt.target.readyState == FileReader.DONE) { // DONE == 2
			filedata=JSON.parse(evt.target.result);
			appVer=parseFloat(userConfig.version.slice(1));
			fileVer=parseFloat(filedata.version.slice(1));
			locationList=filedata.locationsList;
			if ( fileVer<0.516) {
    			objectsList=[];
    			understandDialog('Information','DO NOT PANIC','<b>The world file is created in an older version of the editor.</b><br><br>For now, it is not possible<br>to import older versions of the worlds.<br><br><u>Do not delete your file,<br>because it will be possible soon.</u><hr><br>You can write a message to the creator of the application, which can speed up the above opportunity.<br>For more information, visit the website archonadventures.a-bolt.pl',400);
			} else {
				objectsList=filedata.objects;
			}
			currentLocation=0;
			makeLocationIndex();
			setMapCanvas(locationList[0].width,locationList[0].height);
			drawLocation(0);
			changeTilesSet(locationList[0].tilesSet);
			if ( $('div#dlg-objectLocation').length!=0 ) objectLocationDialog(currentLocation);
		};
		dlgInfo.dialog('close');
	};

    var blob = file.slice(start, stop + 1);
    reader.readAsBinaryString(blob);
}
  
function doLoadWorld() {
	var files=$('#dlg-loadFile #fileChoice').get(0).files;
	if (files.length==1) {
		readBlob(files);
		return true;
	} else {
		understandDialog("Information", "Choice a file", "To download a file, you must first select it by clicking the 'Browse...' button", 400);
		return false;
	}
}

function loadWorld() {
	var dlgInfo=$('<div id="dlg-loadFile" title="Load world project"/>').html(' \
	<div> \
		<label for="projectFileName" class="block">Project file name</label> \
		<input id="fileChoice" type="file" class="hidden"> \
		<input id="projectFileName" type="text" value="'+currentFile+'" readonly class="ui-inputline ui-widget ui-corner-all full-width ui-state-disabled"> \
	</div> \
	').dialog({
		width: 440,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Upload project": function() { if ( doLoadWorld() ) { $(this).dialog('close'); } },
			"Browse...": function() {
				$('#dlg-loadFile #fileChoice').trigger('click');
			},
			"Close": function() { $(this).dialog('close'); },
		},
		create: function () {
			$('#dlg-loadFile #fileChoice').on('change', function () {
				var files=$(this).get(0).files;
				if (files.length==1) {
					$('#dlg-loadFile input#projectFileName').val(files[0].name);
				} else {
					$('#dlg-loadFile input#projectFileName').val('');
				}
			});
		},
		close: function () {
			$(this).dialog('destroy');
		}
	});
	return false;
}

//
//
//

function doSaveWorld() {
	var fileName=$('#dlg-saveFile #projectFileName').val().trim();
	if (fileName=="") {
		understand("Information", "File name can not be empty","",400);
		return false;
	}

	currentFile=fileName;
	currentFileFormat=$('#dlg-saveFile #fileFormat').val();

	if (currentFileFormat=="JSON") {
		var fileContent = JSON.stringify({
			"version":userConfig['version'],
			user:userConfig['user'],
			"locationsList":locationList,
			"objects":objectsList});
	} else {
		return false;
	}

	var bb = new Blob([fileContent], { type: 'text/plain' });
	var a = document.createElement('a');
	a.download = currentFile+"."+currentFileFormat;
	a.href = window.URL.createObjectURL(bb);
	a.click();
	return true;
}

function saveWorld() {
	var dlgInfo=$('<div id="dlg-saveFile" title="Save world project"/>').html(' \
	<div> \
		<label for="projectFileName" class="block">Project file name</label> \
		<input id="projectFileName" type="text" value="'+currentFile+'" class="ui-inputline ui-widget ui-corner-all full-width"> \
		<label for="fileFormat" class="block">Choice file format</label> \
		<select id="fileFormat"> \
			<option value="JSON" selected>JSON text</option> \
			<option value="BIN" disabled>Binary</option> \
		</select> \
	</div> \
	').dialog({
		width: 400,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Make & Download file": function() { if ( doSaveWorld() ) { $(this).dialog('close'); } },
			"Close": function() { $(this).dialog('close'); },
		},
		create: function () {
			$('#dlg-saveFile select#fileFormat').selectmenu();
		},
		close: function () {
			$('#dlg-saveFile select#fileFormat').selectmenu('destroy');
			$(this).dialog('destroy');
		}
	});
	return false;
}