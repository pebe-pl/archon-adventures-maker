// calculate extra values (q1,q2,damage,defense,dexterity) for Armament & Spells
function calculateArmaments() {
    for (armamentName in armaments) {
        var armament=armaments[armamentName];

        for (calcName in armamentStat) {
            var vcalc=armamentStat[calcName](armament);
            armament[calcName]=vcalc;
        }
    }
}

function calculateSpells() {
    for (spellName in spells) {
        var spell=spells[spellName];

        for (calcName in armamentStat) {
            var vcalc=armamentStat[calcName](spell);
            spell[calcName]=vcalc;
        }
    }
}

// calculate characters stats
function calculateCharacters() {
    for (monsterName in monsters) {
        var monster=monsters[monsterName];

        for (calcName in monsterStat) {
            var vcalc=monsterStat[calcName](monster);
            monster[calcName]=vcalc;
        }
    }
}

//

function generateTable(table,data,columns,onRecalc) {
let header,
    list,
    field,
    column,
    entry,
    rowData;

//
const cellEditValue=(e) => {
    let field=$(e.currentTarget),
        rowID=field.data('rowid'),
        columnName=field.data('colname'),
        value=data[rowID][columnName];

    if ( $(field).find('span.value').hasClass('hidden') ) return;

    if (columns[columnName].type=="percentage") value=parseInt(value*100);

    let oinput=$('<input type="number" value="'+value+'" class="mini-input text-center full-width">');
    oinput.on('change',function(e) {
        value=parseInt($(this).val());
        if ( columns[columnName].type=="percentage" ) value=value/100;
        if ( columns[columnName].min!==undefined && value<columns[columnName].min) value=columns[columnName].min
        if ( columns[columnName].max!==undefined && value>columns[columnName].max) value=columns[columnName].max
        data[rowID][columnName]=value;
        refreshTable();
//        e.stopImmediatePropagation();
    });
    oinput.one('blur',function() {
        $(oinput).remove();
        let _field=$(field).find('span.value');
        value=parseInt($(this).val());
        if ( columns[columnName].type=="percentage" ) value=value/100;
        cnt=prepareFieldContent(columns[columnName].type,value);
        _field.html(cnt);
        _field.removeClass('hidden');
    })
    $(field).find('span.value').addClass('hidden');
    field.append(oinput);
    oinput.trigger('focus')
}

//
const cellEditString=(e) => {
    let field=$(e.currentTarget),
        rowID=field.data('rowid'),
        columnName=field.data('colname'),
        value=data[rowID][columnName];

    if ( $(field).find('span.value').hasClass('hidden') ) return;

    let oinput=$('<input type="text" value="'+value+'" class="mini-input text-left full-width">');
    oinput.on('change',function() {
        value=$(this).val();
        data[rowID][columnName]=value;
        refreshTable();
    });
    oinput.one('blur', function() {
        $(oinput).remove();
        let _field=$(field).find('span.value');
        value=$(this).val();
        cnt=prepareFieldContent(columns[columnName].type,value);
        _field.html(cnt);
        _field.removeClass('hidden');
    });
    $(field).find('span.value').addClass('hidden');
    field.append(oinput);
    oinput.trigger('focus')
}

//
const cellEditEnum=(e) => {
    let field=$(e.currentTarget),
        rowID=field.data('rowid'),
        columnName=field.data('colname'),
        value=data[rowID][columnName];

    if ( $(field).find('span.value').hasClass('hidden') ) return;

    let oinput=$('<select id="enum" class="mini-input text-center full-width"/>');
    for ( itemID in columns[columnName].enum ) {
        var itemValue=columns[columnName].enum[itemID];
        var item=$('<option value="'+itemValue+'"/>').html(itemValue)
        if ( itemValue==value ) item.prop('selected',true)
        oinput.append(item);
    }
    oinput.on('change',function() {
        value=$(this).val();
        data[rowID][columnName]=value;
        refreshTable();
    });
    oinput.one('blur', function() {
        $(oinput).remove();
        let _field=$(field).find('span.value');
        value=$(this).val();
        cnt=prepareFieldContent(columns[columnName].type,value);
        _field.html(cnt);
        _field.removeClass('hidden');
    })
    $(field).find('span.value').addClass('hidden');
    field.append(oinput);
    oinput.trigger('focus')
}

//
const cellEditObject=(e) => {
    let field=$(e.currentTarget),
        rowID=field.data('rowid'),
        columnName=field.data('colname'),
        armData=data[rowID].Armament;

    let dlg=$('<div id="dlg-characterArmament" title="Character '+data[rowID].name+' armament"/>');
    dlg.html('<div class="inline-block width-200p">\
    	      <label for="attackList">Attacking hand:</label>\
    	      <ul id="attackList" class="innerlist">\
    	      </ul>\
    	      </div>\
    	      <div class="inline-block width-200p">\
    	      <label for="ammoList">Ammunition:</label>\
    	      <ul id="ammoList" class="innerlist">\
    	          <li class="item" data-data="ammo" data-value="">None</li>\
    	      </ul>\
    	      </div>\
    	      <div class="inline-block width-200p">\
    	      <label for="defenseList">Defending hand:</label>\
    	      <ul id="defenseList" class="innerlist">\
    	          <li class="item" data-data="defense" data-value="">None</li>\
    	      </ul>\
    	      </div>\
    	      <div class="inline-block width-200p">\
    	      <label for="spellList">Spell:</label>\
    	      <ul id="spellList" class="innerlist">\
    	          <li class="item" data-data="spell" data-value="">None</li>\
    	      </ul>\
    	      </div>\
    	    ');

    dlg.dialog({
        autoopen:true,
        width:880,
        resizable:false,
        modal:true,
        buttons:{
            "Use this sets":function() {
                let _field=$(field).find('span.value');
                rowData=data[rowID];
                value=rowData[columnName];
                cnt=prepareFieldContent(columns[columnName].type,value);
                _field.html(cnt);

                $(dlg).dialog('close')  
            }
        },
        close:function() {
            $(dlg).dialog('destroy')
            refreshTable();
        }
    });

    let attackList=$(dlg).find('ul#attackList'),
        defenseList=$(dlg).find('ul#defenseList'),
        ammoList=$(dlg).find('ul#ammoList'),
        spellList=$(dlg).find('ul#spellList');

    for (let armamentID in armaments) {
        var armament=armaments[armamentID]
        var item=$('<li class="item" data-data="'+armament.mode+'" data-value="'+armament.id+'">');
        item.html(armament.name);
        if ( armament.mode=="attack" ) {
            attackList.append(item.addClass('col-red'))
            if ( armData.attack!==undefined && armData.attack==armament.id )
                item.addClass('select')
        }
        if ( armament.mode=="defense" ) {
            defenseList.append(item.addClass('col-green'))
            if ( armData.defense!==undefined && armData.defense==armament.id )
                item.addClass('select')
        }
        if ( armament.mode=="ammo" ) {
            ammoList.append(item.addClass('col-yellow'))
            if ( armData.ammo!==undefined && armData.ammo==armament.id )
                item.addClass('select')
        }
    }
    for (let spellID in spells) {
        let spell=spells[spellID]
        let item=$('<li class="item" data-data="spell" data-value="'+spell.id+'">').html(spell.name);
        if ( spell.mode=="attack" ) item.addClass('col-red')
        if ( spell.mode=="defense" ) item.addClass('col-green')
        if ( armData.spell!==undefined && armData.spell==spell.id )
            item.addClass('select')
        spellList.append(item)
    }

    $(dlg).find('li.item').on('click',function(e) {
        let item=$(e.target),
            mode=$(item).data('data'),
            value=$(item).data('value')

        item.parent().find('li.select').removeClass('select');
        item.addClass('select')
        if (value!="")
            armData[mode]=value
        else
            delete armData[mode]
    })
}

const changeColumnSort=(e) => {
    let ocol=$(e.target),
        columnName=ocol.data('column');

    const sortDesc=(a,b) => {
        if (a[columnName]>b[columnName] ) return -1
        if (a[columnName]<b[columnName] ) return 1
        return 0;
    }
    const sortAsc=(a,b) => {
        if (a[columnName]>b[columnName] ) return 1
        if (a[columnName]<b[columnName] ) return -1
        return 0;
    }
    for (let colName in columns)
        if ( colName!=columnName && columns[colName].sort!==undefined ) delete columns[colName].sort;

    console.log("Sort table by column '"+columnName+"'");

    let ndata=[...data];

    if ( columns[columnName].sort==1 ) {
        columns[columnName].sort=0; ndata.sort(sortDesc)
    } else {
        columns[columnName].sort=1; ndata.sort(sortAsc)
    }
//     list=$(table).find('ul#list').empty();
//    refreshTable();

    let _list=$(table).find('ul#list li').detach();
    for (rowID in ndata) {
        let rowHTML=ndata[rowID]._HTML.rowHTML;
        list.append(rowHTML)
    }
    
}

const prepareFieldContent=(columnType,value) => {
    let cnt="";

    switch(columnType) {
        case "tile": // ...tile field type
            let tile=tiles[value];
            if ( tile!==undefined ) {  // if value is defined, make a tile canvas
                let canvas=$('<canvas id="tileCanvas" width="'+(tile.width*charWidth)+'" height="'+(tile.height*charHeight)+'"/>');
                let ctx=canvas[0].getContext('2d');
                let i=0, oCurrentChars=currentChars;

                if (tile.tileSet==-1)
                    currentChars=0;
                else
                    currentChars=tile.tileSet;

                for(let y=0;y<tile.height;y++) {
                    for(let x=0;x<tile.width;x++) {
                        drawChar(x,y,tile.data[i],ctx);
                        i++;
                    }
                }

                return canvas
//                field.append(canvas)
            }
        break;
        case "object": // object list type
            let _fields=[];
            for (let fieldName in value) {
                let fieldValue=value[fieldName];
                let armData=getArmamentStat(fieldValue);
                switch(fieldName) {
                    case "spell":
                        _fields.push('<span class="col-blue">'+armData.name+'</span>')
                        break;
                    case "attack":
                    if ( fieldName=="attack" )
                        _fields.push('<span class="col-red">'+armData.name+'</span>')
                        break;
                    case "defense":
                        _fields.push('<span class="col-green">'+armData.name+'</span>')
                        break;
                    case "ammo":
                        _fields.push('<span class="col-yellow">'+armData.name+'</span>')
                        break;
                }
            }
            if ( _fields.length!=0 )
                cnt=_fields.join(' ')
            else
                cnt="-"
        break;
        case "string": // simple string type
        case "enum":   // enumerator type
            cnt=value;
        break;
        case "integer": // simple integer type
            value=Math.round(value);
            cnt=parseInt(value);
        break;
        case "float": // float value with precision to 5 digits after coma
            cnt=value.toPrecision(5);
        break;
        case "percentage": // precentage value with "%" sign
            cnt=parseInt(value*100)+"%";
        break;
    }
    return cnt;
}

const recalculateTable=() => {
// calculate extra values & calculate max values for bar visualisation if needed
    onRecalc();
    for (let columnName in columns) {
        let column=columns[columnName]
        if ( column.mode!==undefined && column.mode=="max" ) {
            column.max=0;
            for (let rowID in data) {
                let value=data[rowID][columnName];
                if (value>column.max) column.max=value;
            }
        }
    }
}

const refreshTable=() => {
    recalculateTable();
    for (let rowID in data) {
        let rowData=data[rowID];
        let entry=$('<li id="'+rowID+'"/>');
        for (let columnName in columns) {
            let column=columns[columnName];
            let value=rowData[columnName];
            if ( column.mode!==undefined && column.mode=="max") {
                let cnt=prepareFieldContent(column.type,value);

                let _field=rowData['_HTML'][columnName];
                let _bar=_field.find('div.stats');
                let _label=_field.find('div.label');
                if (cnt==0) cnt="";
                _label.html(cnt);
                let _v=((value/column.max)*100);
                _bar.progressbar({value:_v});
            }
        }
    }
//    applyEvents();
}

const applyEvents=() => {
    $(list).find('span.field.editable').on('click',function(e) {
        let fieldType=$(e.currentTarget).data('edit');
        switch (fieldType) {
            case "integer": // cell edit - value
            case "float":
            case "percentage": 
                cellEditValue(e);
            break;
            case "string":
                cellEditString(e)
                break;
            case "enum":
                cellEditEnum(e)
            break;
            case "object": // cell edit - object type
                cellEditObject(e)
            break;
        }
    })
}
//
//
//
    recalculateTable();

// make header
    header=$(table).parent().find('ul#header li').empty();
    for (let columnName in columns) {
        let column=columns[columnName]
        var item=$('<span class="field editable" data-column="'+columnName+'"/>')
            .addClass(columnName)
            .html(column.title);
        header.append(item);
        item.on("click",changeColumnSort);
    }
//
// Makeing a table contnet
//
    list=$(table).find('ul#list').empty();
    for (rowID in data) {
        rowData=data[rowID];
        entry=$('<li id="'+rowID+'"/>');
        rowData._HTML=[];
        rowData._HTML['rowHTML']=entry;
        for (columnName in columns) {
            column=columns[columnName];
            let value=rowData[columnName];
            field=$('<span class="field '+columnName+'" data-rowID="'+rowID+'" data-colName="'+columnName+'" data-edit="'+column.type+'"/>');
            rowData._HTML[columnName]=field;
            let cnt="";

//
// make content for a filed...
//
            if ( value!=null ) {
                cnt=prepareFieldContent(column.type,value)
//
// calculate relative value for "maximum" presentation mode
// !! In this mode, value can't by editable !!
//
                if ( column.mode!==undefined && column.mode=="max") {
                    if (cnt==0) cnt="";
                    let _barclass=(column.class!==undefined?" "+column.class:"")
                    let _bar=$('<div class="stats'+_barclass+'"><div class="label">'+cnt+'</div></div>');
                    let _v=((value/column.max)*100);
                    _bar.progressbar({value:_v});
                    field.append(_bar).addClass('bar')
                } else {
//
// if "maximum" mode is not set,
// show prepared value and make it editable
//
                    var ovalue=$('<span class="value"/>').html(cnt);
                    field.addClass('editable').append(ovalue);
                }
            } else { // when value is null, show minus character
                field.html('-');
            }
            entry.append(field);
        }
        list.append(entry);
    }
    applyEvents();
}

function dialogGeneralClassification() {
    var dlg=$('<div id="dlg-generalClassification" title="General battlefield classification"/>');

    dlg.html('<div id="group-tabs">\
                <ul>\
                    <li><a href="#armaments">Armament</a></li>\
                    <li><a href="#spells">Spells</a></li>\
                    <li><a href="#characters">Characters</a></li>\
                </ul>\
                <div id="armaments">\
                    <ul id="header">\
                    <li></li>\
                    </ul>\
                    <div id="armaments-list" class="table">\
                        <ul id="list"></ul>\
                    </div>\
                </div>\
                <div id="spells">\
                    <ul id="header">\
                    <li></li>\
                    </ul>\
                    <div id="spells-list" class="table">\
                        <ul id="list"></ul>\
                    </div>\
                </div>\
                <div id="characters">\
                    <ul id="header">\
                    <li></li>\
                    </ul>\
                    <div id="characters-list" class="table">\
                        <ul id="list"></ul>\
                    </div>\
                </div>\
              </div>');

    dlg.dialog({
        autoopen:true,
        width:1050,
        height:570,
        resizable:false,
        modal:true,
        buttons:{
            'Close': function() { $(this).dialog('close'); }
        },
        create:function() {
            $(dlg).find('div#group-tabs').tabs();

            var armamentList=$(dlg).find('div#armaments-list');
            var armamentTable=new generateTable(armamentList,armaments,{
                'tile':{title:"Tile",type:"tile"},
                'name':{title:"Name",type:"string"},
                'mode':{title:"Mode",type:"enum",enum:['attack','defense','ammo']},
                'Force':{title:"Force",type:"integer",step:1,min:0},
                'Speed':{title:"Speed",type:"integer",step:1,min:0},
                'Interval':{title:"Interval",type:"percentage",step:0.05,min:0,max:1},
                'Duration':{title:"Duration",type:"integer",step:5,min:5,max:100},
                'damage':{title:"Damage",type:"integer",mode:"max",class:"red"},
                'defense':{title:"Defense",type:"integer",mode:"max",class:"green"},
                'dexterity':{title:"Dexterity",type:"integer",mode:"max",class:"yellow"}
            },calculateArmaments)

            var spellsList=$(dlg).find('div#spells-list');
            var spellTable=new generateTable(spellsList,spells,{
                'tile':{title:"Tile",type:"tile"},
                'name':{title:"Name",type:"string"},
                'mode':{title:"Mode",type:"enum",enum:['attack','defense','ammo']},
                'Force':{title:"Force",type:"integer",step:1,min:0},
                'Speed':{title:"Speed",type:"integer",step:1,min:0},
                'Interval':{title:"Interval",type:"percentage",step:0.05,min:0,max:1},
                'Duration':{title:"Duration",type:"integer",step:5,min:5,max:100},
                'damage':{title:"Damage",type:"integer",mode:"max",class:"red"},
                'defense':{title:"Defense",type:"integer",mode:"max",class:"green"},
                'dexterity':{title:"Dexterity",type:"integer",mode:"max",class:"yellow"}
            },calculateSpells)

            var charactersList=$(dlg).find('div#characters-list');
            var charactersTable=new generateTable(charactersList,monsters,{
                'tile':{title:"Tile",type:"tile"},
                'name':{title:"Name",type:"string"},
                'Side':{title:"Side",type:"enum",enum:['Light','Dark']},
                'Lifespan':{title:"Lifespan",type:"integer",step:1},
                'Range':{title:"Range",type:"integer",step:1},
                'Speed':{title:"Speed",type:"percentage",step:0.05,min:0,max:2},
                'Armament':{title:"Armament",type:"object"},
                'health':{title:"Health",type:"integer",mode:"max",class:""},
                'damage':{title:"Damage",type:"integer",mode:"max",class:"red"},
                'defense':{title:"Defense",type:"integer",mode:"max",class:"green"},
                'dexterity':{title:"Dexterity",type:"integer",mode:"max",class:"yellow"}
            },calculateCharacters)
//              console.log($('div#characters-list ul#list').niceScroll());
        },
        close:function() {
            $(dlg).find('div#group-tabs').tabs('destroy');
            dlg.dialog('destroy');
        }
    })
}

function initGeneralClassification() {

}