var tilesSetName={0:'Dungeon',1:'Outside',"length":2};

var tiles={	"clear":				{"tileSet":-1,"group":"global", "width":2, "height":1, "data":[0,0]},

			"wallv":				{"tileSet":0,"group":"dungeon", "width":2, "height":1, "data":[30,30]},
			"wallh_left_corner":	{"tileSet":0,"group":"dungeon", "width":2, "height":1, "data":[1,2]},
			"wall":					{"tileSet":0,"group":"dungeon", "width":2, "height":1, "data":[2,2]},
			"wall_right_corner":	{"tileSet":0,"group":"dungeon", "width":2, "height":1, "data":[2,3]},
			"pillar_left":			{"tileSet":0,"group":"dungeon", "width":2, "height":1, "data":[5,1]},
			"pillar_right":			{"tileSet":0,"group":"dungeon", "width":2, "height":1, "data":[3,5]},
			"window_small":			{"tileSet":0,"group":"dungeon", "width":1, "height":1, "data":[23]},
			"torch":				{"tileSet":0,"group":"dungeon", "width":1, "height":1, "data":[32,33,34,35]},
			"arc_small":			{"tileSet":0,"group":"dungeon", "width":2, "height":1, "data":[6,8]},
			"arc_middle":			{"tileSet":0,"group":"dungeon", "width":3, "height":1, "data":[6,7,8]},
			"door_h_close":			{"tileSet":0,"group":"dungeon", "width":2, "height":1, "active":true, "data":[9,10]},
			"door_h_close_bwall":	{"tileSet":0,"group":"dungeon", "width":4, "height":1, "data":[3,9,10,1]},
			"door_h_open":			{"tileSet":0,"group":"dungeon", "width":2, "height":1, "active":true, "data":[41,42]},
			"door_h_open_bwall":	{"tileSet":0,"group":"dungeon", "width":4, "height":1, "data":[3,41,42,1]},
			"door_v_close":			{"tileSet":0,"group":"dungeon", "width":2, "height":2, "active":true, "data":[11,12,13,14]},
			"door_v_open_left":		{"tileSet":0,"group":"dungeon", "width":3, "height":2, "active":true, "data":[16,17,18,0,29,31]},
			"door_v_open_left_wall":{"tileSet":0,"group":"dungeon", "width":3, "height":2, "active":true, "data":[26,27,28,0,29,31]},
			"stairs_up_left":		{"tileSet":0,"group":"dungeon", "width":3, "height":2, "active":true, "data":[50,51,52,47,48,49]},
			"stairs_up_left_wall":	{"tileSet":0,"group":"dungeon", "width":3, "height":2, "active":true, "data":[76,77,78,47,48,49]},
			"stairs_down_left":		{"tileSet":0,"group":"dungeon", "width":5, "height":2, "active":true, "data":[30,30,80,80,81 ,30,30,61,62,63]},
			"stairs_up_right":		{"tileSet":0,"group":"dungeon", "width":3, "height":2, "active":true, "data":[55,56,57, 58,59,60]},
			"stairs_up_right_wall":	{"tileSet":0,"group":"dungeon", "width":3, "height":2, "active":true, "data":[93,94,95, 58,59,60]},
			"stairs_down_right":	{"tileSet":0,"group":"dungeon", "width":5, "height":2, "active":true, "data":[79,80,80,30,30 ,44,45,46,30,30]},

			"grass1":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[1,2]},
			"grass2":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[3,4]},
			"grass3":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[5,6]},
			"stone1":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[53,54]},
			"stone2":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[55,56]},
			"stone3":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[57,58]},
			"bush1":			{"tileSet":1,"group":"outside", "width":1, "height":1, "data":[32]},
			"bush2":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[33,34]},
			"bush3":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[35,36]},
			"tree1":			{"tileSet":1,"group":"outside", "width":2, "height":1, "data":[37,38]},
			"tree2":			{"tileSet":1,"group":"outside", "width":2, "height":2, "data":[7,8,39,40]},
			"tree3":			{"tileSet":1,"group":"outside", "width":3, "height":2, "data":[9,10,11,41,42,43]},
			"tree4":			{"tileSet":1,"group":"outside", "width":3, "height":2, "data":[12,13,14,44,45,46]},

			"item_chest_close":	{"tileSet":-1,"group":"items", "width":2, "height":1, "active":true, "data":[96,97]},
			"item_chest_open":	{"tileSet":-1,"group":"items", "width":2, "height":1, "active":true, "data":[98,99]},
			"item_scroll":		{"tileSet":-1,"group":"items", "width":2, "height":1, "active":true, "data":[100,101]},
			"item_elixir":		{"tileSet":-1,"group":"items", "width":2, "height":1, "active":true, "data":[102,103]},
			"item_key":			{"tileSet":-1,"group":"items", "width":2, "height":1, "active":true, "data":[104,105]},
			"item_gold":		{"tileSet":-1,"group":"items", "width":2, "height":1, "active":true, "data":[106,107]},
			"item_skull":		{"tileSet":-1,"group":"items", "width":2, "height":1, "active":true, "data":[108,109]},
			"weapon_shieldsmall":{"tileSet":-1,"group":"weapons", "width":2, "height":1, "active":true, "data":[110,111]},
			"weapon_shieldbig":	{"tileSet":-1,"group":"weapons", "width":2, "height":1, "active":true, "data":[112,113]},
			"weapon_arrows":	{"tileSet":-1,"group":"weapons", "width":2, "height":1, "active":true, "data":[114,115]},
			"weapon_spear":		{"tileSet":-1,"group":"weapons", "width":2, "height":1, "active":true, "data":[116,117]},
			"weapon_axe":		{"tileSet":-1,"group":"weapons", "width":2, "height":1, "active":true, "data":[118,119]},
			"weapon_sword":		{"tileSet":-1,"group":"weapons", "width":2, "height":1, "active":true, "data":[120,121]},
			"weapon_bow":		{"tileSet":-1,"group":"weapons", "width":2, "height":1, "active":true, "data":[122,123]},

			"monster_banshee":		{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+96,256+97]},
			"monster_saurian":		{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+98,256+99]},
			"monster_troll":		{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+100,256+101]},
			"monster_goblin":		{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+102,256+103]},
			"monster_sorceress":	{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+104,256+105]},
			"monster_basilisk":		{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+106,256+107]},
			"monster_manticore":	{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+108,256+109]},
			"monster_shapeshifter":	{"tileSet":-1,"group":"monsters", "width":2, "height":1, "active":true, "data":[256+110,256+111]}
		  };

var objTiles={
//
//
//
	"door":{
		'lists':['doorRequirementsList'],
        "HTML":'<legend class="full-width text-center">Action parameters</legend>\
						<div class="full-width text-center"><label for="object_data_objID">What is needed to open the door?</label>\
						<select id="object_data_openMethod" class="objField">\
						<option value="" disabled selected>Choice method...</option>\
						<option value="nothing">Door opens freely</option>\
						<option value="item">Item(s) needed</option>\
						</select>\
						<ul id="doorRequirementsList" class="innerlist">\
							<li id="newItemObject" class="item command">Add item object...</li>\
						</ul>\
						</div>',
		"make":function(dlg,obj) {
			var setMethod=function(v) {
				if (v=="nothing" || v=="") {
					$(dlg).find('ul#doorRequirementsList').addClass('hidden');
				}
				if (v=="item") {
					$(dlg).find('ul#doorRequirementsList').removeClass('hidden');
					$(dlg).find('li#newItemObject').removeClass('hidden');
				}
    		}

			var list=$(dlg).find('select#object_data_openMethod');
			var openMethod=obj.data['openMethod'];
			if ( openMethod!==undefined ) {
				$(list).find('option[value="'+openMethod+'"]').prop('selected',true);
			} else
			    openMethod="";
			setMethod(openMethod);
			list.selectmenu({
				width:250,
				change:function(e,o) {
					openMethod=o.item.value;
					setMethod(openMethod);
				}
			});

			var doorRequirementsList=$(dlg).find('ul#doorRequirementsList')
			if (obj.data.doorRequirementsList!==undefined) {
				for (entry in obj.data.doorRequirementsList) {
					var objID=obj.data.doorRequirementsList[entry];
					createInternalListItem({
						list:doorRequirementsList,
						field:"doorRequirementsList",
						locationID:objectsList[objID].locationid,
						obj:objectsList[objID],
						deleteObject:false,
						editable:false
					});
				}
			}

    		$(dlg).find("li#newItemObject").click(function() {
				dialog_choiceObject({
					filters:['item'],
					onChoice:function(obj) {
						var itemList=createInternalListItem({list:doorRequirementsList,field:"doorRequirementsList",locationID:obj.locationID,obj:obj,deleteObject:false,editable:false});
				    }
				});
			})
		}
	},

//
//
//
	"stairs":{
		"HTML":'<div class="full-width"><label for="object_data_target">Target:</label>\
		        <input id="object_data_target" type="hidden" value="" class="objField">\
		        <input id="target" type="text" readonly class="ui-inputline ui-widget ui-corner-all full-width">\
		        <button id="but_choiceTarget" class="ui-button ui-widget ui-cornet-all">Choice target...</button>\
		        </div>',
		"make":function(dlg,obj) {
			$(dlg).find("button#but_choiceTarget").click(function() {
				dialog_choiceObject({
					filters:['stairs'],
					onChoice:function(obj) {
						$(dlg).find('#object_data_target').val(obj.ID);
						$(dlg).find('#target').val(' #'+obj.ID+' '+obj.name+' in '+locationList[obj.locationID].name);
				    }
				});
			})
		}
	},

//
//
//
	"elixir":{
		"HTML":'<legend class="full-width text-center">Elixir parameters:</legend>\
					<div class="full-width text-center">\
						<label for="object_data_fluidType" class="full-width text-center">Fluid type:</label>\
						<select id="object_data_fluidType" class="objField">\
							<option value="" disabled selected>Choice...</option>\
							<option value="health">Health</option>\
							<option value="magic">Magic</option>\
							<option value="poison">Poison</option>\
							<option value="antidote">Antidote</option>\
						</select>\
					</div>\
					<div class="full-width text-center">\
						<label for="object_data_capacity" class="full-width text-center">Capacity:</label>\
						<select id="object_data_capacity" class="objField">\
							<option value="" disabled selected>Choice...</option>\
							<option value="small">Small</option>\
							<option value="medium">Medium</option>\
							<option value="large">Large</option>\
						</select>\
					</div>',
		"make":function(dlg,obj) {
			var elixirType=obj.data['fluidType'];
			if ( elixirType!==undefined ) {
				$(dlg).find('select#object_data_fluidType option[value="'+elixirType+'"]').prop('selected',true);
			}
			var elixirCapacity=obj.data['capacity'];
			if ( elixirCapacity!==undefined ) {
				$(dlg).find('select#object_data_capacity option[value="'+elixirCapacity+'"]').prop('selected',true);
			}

			$(dlg).find('select').selectmenu({width:250});
		}
	},

//
//
//
	"gold":{
		"HTML":'<label for="object_data_amount">Amount of gold:</label>\
				<input id="object_data_amount" type="number" value="" min="0" max="99999" class="objField ui-inputline ui-widget ui-corner-all width-100p">',
		"make":function(dlg,obj) {
			var amount=obj.data['amount'];
			if (amount===undefined) amount="";
			$(dlg).find('#object_data_amount').val(amount);
		}
	},

    "key":{
    	"HTML":'<div class="ui-state-error text-center">This item has no properties</div>'
    },
//
//
//
	"chest":{
	    'lists':['contentsList'],
		"HTML":'<label for="contentsList">Items inside chest</label>\
				<ul id="contentsList" class="innerlist">\
					<li id="newItem" class="item command">Add new item...</li>\
				</ul>',
		"make":function(dlg,obj) {
			var contentsList=$(dlg).find('ul#contentsList');
			if (obj.data.contentsList!==undefined) {
				for (entry in obj.data.contentsList) {
					var objID=obj.data.contentsList[entry];
					createInternalListItem({
						list:contentsList,
						field:"contentsList",
						obj:objectsList[objID],
						deleteObject:true,
						editable:true
					});
				}
			}
			$(contentsList).find('li#newItem').click(function() {
				dialog_makeObject(function(item) {
					var nobj=makeObject({
						objType:item,
						autoedit:true,
						onMake:function(obj) {
        					createInternalListItem({list:contentsList,field:"contentsList",obj:nobj,deleteObject:true,editable:true});
						}
					});
				});
			});

		}
	},

//
//
//
	"scroll":{
		'lists':['questRequirementsList','questRewardList'],
		"HTML":'<label for="object_data_scrollType">Scroll type:</label>\
					<select id="object_data_scrollType" class="objField">\
						<option value="info">Information</option>\
						<option value="spell">Spell</option>\
						<option value="quest">Quest</option>\
					</select>\
					<div id="scroll-tabs" class="tabs">\
						<ul>\
							<li><a href="#tab-scroll-description">Description</a></li>\
							<li><a href="#tab-scroll-spell">Spell</a></li>\
							<li><a href="#tab-scroll-quest">Quest</a></li>\
						</ul>\
						<div id="tab-scroll-description" class="text-center">\
							<textarea id="object_data_description" cols="32" rows="4" maxlength="256" wrap="hard" class="objField ui-inputline ui-widget ui-corner-all "></textarea>\
						</div>\
						<div id="tab-scroll-spell" class="text-center">\
							<select id="object_data_spell" class="objField">\
								<option value="" disabled selected >Select from list...</option>\
								<option value="imprison">Imprison</option>\
								<option value="teleport">Teleport</option>\
								<option value="heal">Heal</option>\
								<option value="exchange">Exchange</option>\
								<option value="" disabled>- Attack spells:</option>\
								<option value="energy_bolt">Energy Bolt</option>\
								<option value="fireball">Fireball</option>\
								<option value="whirlwind">Whirlwind</option>\
								<option value="eye_beam">Eye Beam</option>\
								<option value="lighting_bolt">Lighting Bolt</option>\
								<option value="tile_spikes">Tile Spikes</option>\
								<option value="boulder">Bulder</option>\
								<option value="fiery_breath">Fiery Breath</option>\
								<option value="scream">Scream</option>\
							</select>\
						</div>\
						<div id="tab-scroll-quest" class="text-center">\
							<div id="quest-tabs" class="tabs">\
								<ul>\
									<li><a href="#quest-requirements">Requirements</a></l1>\
									<li><a href="#quest-reward">Reward</a></l1>\
								</ul>\
								<div id="quest-requirements">\
									<ul id="questRequirementsList" class="innerlist">\
										<li id="newRequirementObject" class="item command">Add new requirement object...</li>\
									</ul>\
								</div>\
								<div id="quest-reward">\
									<ul id="questRewardList" class="innerlist">\
										<li id="newRewardItem" class="item command">Add new reward item...</li>\
									</ul>\
								</div>\
							</div>\
						</div>\
					</div>',
		"make":function(dlg,obj) {
			$(dlg).find('div.tabs').tabs();

			var description=obj.data['description'];
			if ( description!==undefined ) {
				$(dlg).find('textarea#object_data_description').val(description);
			}

			var scrollType=obj.data['scrollType'];
			if ( scrollType!==undefined ) {
				$(dlg).find('select#object_data_scrollType option[value="'+scrollType+'"]').prop('selected',true);
			}

			$(dlg).find('select#object_data_scrolltype')
				.selectmenu({
					width:150,
					change:function(e,o) {
						var item=o.item.value;
						var o=$(dlg).find('#scroll-tabs li > a.can');
						$(o).addClass('ui-state-disabled');
						var o=$(dlg).find('#scroll-tabs li > a#'+item);
						$(o).removeClass('ui-state-disabled');
					}
				});

			var spell=obj.data['spell'];
			if ( spell!==undefined ) {
				$(dlg).find('select#object_data_spell option[value="'+spell+'"]').prop('selected',true);
			}
			$(dlg).find('select#object_data_spell').selectmenu({width:250});

			// requirementsList prepare
			var requirementsList=$(dlg).find('ul#questRequirementsList')
			if (obj.data.requirementsList!==undefined) {
				for (entry in obj.data.requirementsList) {
					var objID=obj.data.requirementsList[entry];
					createInternalListItem({
						list:requirementsList,
						field:"requirementsList",
						locationID:objectsList[objID].locationid,
						obj:objectsList[objID],
						deleteObject:false,
						editable:false
					});
				}
			}
			$(requirementsList).find('li#newRequirementObject')
			.click(function() {
				dialog_choiceObject({
					onChoice:function(obj) {
					    var itemList=createInternalListItem({list:requirementsList,field:"requirementsList",locationID:locationID,obj:obj,deleteObject:false,editable:false});
				    }
				});
			});

			// rewardsList prepare
			var rewardsList=$(dlg).find('ul#questRewardList');
			if (obj.data.rewardsList!==undefined) {
				for (entry in obj.data.rewardsList) {
					var objID=obj.data.rewardsList[entry];
					createInternalListItem({
						list:rewardsList,
						field:"rewardsList",
						locationID:objectsList[objID].locationid,
						obj:objectsList[objID],
						deleteObject:true,
						editable:true
					});
				}
			}
			$(rewardsList).find('li#newRewardItem')
			.click(function() {
				dialog_makeObject(function(item) {
					var nobj=makeObject({
						objType:item,
						autoedit:true,
						onMake:function(obj) {
        					createInternalListItem({list:rewardsList,field:"rewardsList",obj:nobj,deleteObject:true,editable:true});
						}
					});
				});
			});
		}
	},
///
/// weapons
///
    
///
/// monsters
///
    "monster":{
    	dlgWidth:650,
    	'lists':['itemsRewardList'],
/*
    	HTML:'<div class="inline-block">\
				  <div class="text-right"><label for="object_data_name">Name:</label>\
				  <input id="object_data_name" type="text" value="" maxlength=32 class="objField ui-inputline ui-widget ui-corner-all width-150p"></div>\
					\
				  <div class="text-right"><label for="object_data_weapon">Weapon:</label>\
				  <select id="object_data_weapon" class="objField"></select>\
				  </div>\
			  </div>\
    	      <div class="inline-block float-right">\
    	          <div>\
				  <label for="object_data_health" class="inline-block width-75p">Health</label>\
				  <label for="object_data_dexterity" class="inline-block width-75p">Dexter.</label>\
				  <label for="object_data_damage" class="inline-block width-75p">Damage</label>\
				  <label for="object_data_defense" class="inline-block width-75p">Defense</label>\
				  </div>\
				  <div>\
				  <input id="object_data_health" class="objField ui-inputline ui-widget ui-corner-all text-center width-75p">\
				  <input id="object_data_dexterity" class="objField ui-inputline ui-widget ui-corner-all text-center width-75p">\
    			  <input id="object_data_damage" class="objField ui-inputline ui-widget ui-corner-all text-center width-75p">\
				  <input id="object_data_defense" class="objField ui-inputline ui-widget ui-corner-all text-center width-75p">\
				  </div>\
  				  <button id="generalClassification" class="ui-button ui-widget">General Classification...</button>\
				  <div class="text-right">\
				  <label for="object_data_level">Level:</label>\
				  <input id="object_data_level" class="objField ui-inputline ui-widget ui-corner-all text-center width-100p">\
				  <label for="rank">Rank</label>\
				  <input id="rank" type="text" value="" readonly tabindex="-1" class="objField text-center ui-inputline ui-widget ui-corner-all width-100p ui-state-disabled">pts\
				  </div>\
				  \
				  <div class=""><label for="chanceMaxDamage">Maximum damage chance:</label>\
				  <div id="chanceMaxDamage" class="stats"><div class="label"></div></div>\
				  </div>\
				  <div class=""><label for="chanceFightBack">Fight back chance:</label>\
				  <div id="chanceFightBack" class="stats"><div class="label"></div></div>\
				  </div>\
    	      </div>\
    	      <div class="inline-block">\
    	      <label for="itemsRewardList">Rewards items:</label>\
    	      <ul id="itemsRewardList" class="innerlist">\
					<li id="newRewardItem" class="item command">Add item...</li>\
    	      </ul>\
    	      </div>',
*/    	calculateStat: function(monsterStat,weaponStat,level=1) {
			var generalStat={};
			generalStat.level=level;

			for (fieldName in statValueCalculate) {
				var calcFn=statValueCalculate[fieldName];
				generalStat[fieldName]=calcFn(monsterStat,weaponStat)*level;
			}

            generalStat['general']={};
			generalStat['general']['maxDamage']=statGeneralCalculate['maxDamage'](generalStat);
			generalStat['general']['fightBack']=statGeneralCalculate['fightBack'](generalStat);
			generalStat['general']['rank']=statGeneralCalculate['rank'](generalStat);
			return generalStat;
    	},
       	setFieldsValue: function(dlg,generalStat) {
			$(dlg).find('input#object_data_level').val(parseInt(generalStat.level));

			for (fieldName in statValueCalculate) {
				$(dlg).find('input#object_data_'+fieldName).val(parseInt(generalStat[fieldName]));
			}
			v=parseInt(generalStat['general']['maxDamage']);
			$(dlg).find('div#chanceMaxDamage').progressbar({value:v});
			$(dlg).find('div#chanceMaxDamage div.label').text(v);
			v=generalStat['general']['fightBack'];
			$(dlg).find('div#chanceFightBack').progressbar({value:v});
			$(dlg).find('div#chanceFightBack div.label').text(v);
			v=generalStat['general']['rank'];
			$(dlg).find('input#rank').val(v);
    	},
    	make:function(dlg,obj) {
/*
			var monsterName=obj.tile.slice(obj.tile.indexOf("_")+1);
    		var monsterStat=monsters[monsterName];

            var weaponList=$(dlg).find('select#object_data_weapon')
    		for (weaponID in weapons) {
    			var item=$('<option value="'+weaponID+'"/>').html(weapons[weaponID].name);
    			weaponList.append(item);
    		}

			var weapon=obj.data['weapon'];
			if ( weapon===undefined ) weapon=monsterStat.Weapon;
            var weaponStat=weapons[weapon];
			$(dlg).find('select#object_data_weapon option[value="'+weapon+'"]').prop('selected',true);
			$(dlg).find('select#object_data_weapon').selectmenu({
				width:175,
				change:(e,o) => {
					var weapon=o.item.value;
                    weaponStat=weapons[weapon];
					var generalStat=this.calculateStat(monsterStat,weaponStat);
					this.setFieldsValue(dlg,generalStat);
				}
			});

            var generalStat=this.calculateStat(monsterStat,weaponStat);

			$('input#object_data_level').spinner({min:1,max:99})
			.on('spin',(e,u) => { 
			    var level=u.value;
				var general=this.calculateStat(monsterStat,weaponStat,level); 
				this.setFieldsValue(dlg,general);
			})
			.on('spinchange',(e,u) => {
			    var level=$('input#object_data_level').val();
				var general=this.calculateStat(monsterStat,weaponStat,level); 
				this.setFieldsValue(dlg,general);
			})
			
            this.setFieldsValue(dlg,generalStat);

			// rewardsList prepare
			var itemsRewardList=$(dlg).find('ul#itemsRewardList');
			if (obj.data.itemsRewardList!==undefined) {
				for (entry in obj.data.itemsRewardList) {
					var objID=obj.data.itemsRewardList[entry];
					createInternalListItem({
						list:itemsRewardList,
						field:"itemsRewardList",
						locationID:objectsList[objID].locationid,
						obj:objectsList[objID],
						deleteObject:true,
						editable:true
					});
				}
			}

			$(itemsRewardList).find('li#newRewardItem')
			.click(function() {
				dialog_makeObject(function(item) {
					var nobj=makeObject({
						objType:item,
						autoedit:true,
						onMake:function(obj) {
        					createInternalListItem({list:itemsRewardList,field:"itemsRewardList",obj:nobj,deleteObject:true,editable:true});
						}
					});
				});
			});
*/
    	}
    }
}