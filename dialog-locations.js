var locationList=[],
	currentLocation;

function showLocationDialog() {
	$('#dlg-locations').dialog('open');
}

function makeLocationListEntry(id,name) {
	var width=locationList[id].width,
		height=locationList[id].height,
		tilesSet=locationList[id].tilesSet || 0;
		
	var entry=$('<li class="item" id="location--'+id+'"/>')
	.html(name+'<span>'+tilesSetName[tilesSet]+" "+width+'x'+height+'</span>');
	$('#dlg-locations #list-locations').append(entry);
	entry.on('click',selectLocation);
	entry.on('dblclick',function() { showObjectLocationDialog(currentLocation); });
    $("dlg-locations").niceScroll().resize();
	return entry;
}

function makeLocationIndex() {	
	$('#dlg-locations ul#list-locations').empty();
	
	locationList.forEach(function(locationInfo,index) {
		var entry=makeLocationListEntry(index,locationInfo.name);
		if (index==currentLocation) {
			entry.addClass('select');
		}
	});
}

function makeLocation(name,width,height,tilesSet) {
	var data=[];
	for (i=0;i<width*height;i++) {
		data[i]=0;
	}
	var a=locationList.push({"name":name,"width":width,"height":height,"tilesSet":tilesSet,"dataMap":data});
	$('#dlg-locations #list-locations li.item').removeClass('select');;
	makeLocationListEntry((a-1),name).addClass('select');
	return (a-1);
}

function drawLocation(id) {
	mapWidth=locationList[id].width;
	mapHeight=locationList[id].height;
	dataMap=locationList[id].dataMap;
	currentChars=locationList[id].tilesSet;
	
	for (y=0;y<mapHeight;y++) {
		var adr=(y*mapWidth);
		for (x=0;x<mapWidth;x++) {
			drawChar(x,y,dataMap[adr],map);
			adr++;
		}
	}
}

//
//
//

function createNewLocation(name,width,height,tilesSet=0) {
	// create location
	currentLocation=makeLocation(name,width,height,tilesSet);
	// move to editor
	dataMap=locationList[currentLocation].dataMap;
	// tiles set
	currentChars=tilesSet;
	// make map environment
	setMapCanvas(width,height);
	changeTilesSet(tilesSet)
}

function doCreateLocation(name,width,height,tilesSet=0) {
	name=name.trim();
	if ( name=='' ) {
		understandDialog('Information','Location name can not by empty','',500);
		return false;
	}
	if ( (width<32 || width>256) || (height<8 || height>256) ) {
		understandDialog('Information','Map size is incorredt','The map dimensions must be in the range:<br>32 <width <256; 8 <height <256');
		return false;
	}
	createNewLocation(name,width,height,tilesSet);
	return true;
}

function createLocationDialog(name,width,height,tilesSet=0) {
	if ( name===undefined ) name="Noname";
	if ( width===undefined ) width=64;
	if ( height===undefined ) height=32;
	
	if ( userConfig['info_characterSetOnCreateOnly'] ) {
		var infoField='<p class="ui-state-error"><span id="button_hidemsg" class="ui-icon ui-icon-delete button"></span>The character set can <u>ONLY</u> be set when creating a new location.</p>';
	} else {
		infoField='';
	}

	$('<div id="dlg-createLocation" title="Create new location"/>')
	.html('<label for="newLocationName" class="block">Location name</label> \
		   <input id="newLocationName" type="text" value="'+name+'" class="ui-inputline ui-widget ui-corner-all full-width"> \
		   <fieldset class="text-center"><legend>Location character set:</legend> \
			   <select id="newLocationTilesSet"> \
				   <option value="0">Dungeon</option> \
				   <option value="1">Outside</option> \
			   </select> \
			   '+infoField+' \
		   </fieldset> \
		   <fieldset class="text-center"><legend>Map size:</legend> \
			   <fieldset><legend>Width</legend><input id="newLocationWidth" type="number" value="'+width+'" min="32" max="256" class="ui-inputline ui-widget ui-corner-all width-100p"></fieldset> \
			   <fieldset><legend>Height</legend><input id="newLocationHeight" type="number" value="'+height+'" min="8" max="256" class="ui-inputline ui-widget ui-corner-all width-100p"></fieldset> \
		   </fieldset> \
	')
	.dialog({
		width: 330,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Don't do this": function() { $(this).dialog('close'); },
			"Make": function() { 
				var name=$('#dlg-createLocation #newLocationName').val();
				var width=$('#dlg-createLocation #newLocationWidth').val();
				var height=$('#dlg-createLocation #newLocationHeight').val();
				var tilesSet=$('#dlg-createLocation #newLocationTilesSet').val();
				if ( doCreateLocation(name,width,height,tilesSet) ) {
					$(this).dialog('destroy');
				}
			}
		},
		create:function() {
			if ( userConfig['info_characterSetOnCreateOnly'] ) {
				$('div#dlg-createLocation').find('#button_hidemsg').click(function() {
					$(this).parent().remove();
					userConfig['info_characterSetOnCreateOnly']=false;
					saveConfig();
				});
			}
			
			$('#dlg-createLocation #newLocationTilesSet').selectmenu({width:250});
		},
		close: function () {
			$(this).dialog('destroy');
		}

	});
}

//
//
//

function selectLocation(e) {
	var locationId=e.currentTarget.id;
	if ( locationId.substring(0,10)!="location--" ) return;
	id=parseInt(locationId.substring(10)) || 0;
	$('#dlg-locations #list-locations li.item').removeClass('select');;
	$('#dlg-locations #list-locations li#location--'+id).addClass('select');
	currentLocation=id
	var location=locationList[currentLocation];
	dataMap=location.dataMap;
	setMapCanvas(location.width,location.height);
	drawLocation(currentLocation);
	changeTilesSet(location.tilesSet);
	if ( $('#dlg-objectLocation').length!=0 ) {
		showObjectLocationDialog(currentLocation);
	}
}

//
//
//

function menuLocationsDialog(e) {
	var x=e.pageX, y=e.pageY;
	
	overlay_menu=$('div#locations-menu');
	overlay=$('<div class="menu-overlay"/>').click(function() { 
		overlay.remove(); overlay_menu.addClass('hidden');
	}).appendTo('body');

	var o=$('#dlg-locations ul.selectList li.item.select');
	if (o.length==0) {
		$('div#locations-menu li#cm-propertiesLocation').addClass('ui-state-disabled');
		$('div#locations-menu li#cm-deleteLocation').addClass('ui-state-disabled');
		$('div#locations-menu li#cm-objectLocation').addClass('ui-state-disabled');
	} else {
		$('div#locations-menu li#cm-propertiesLocation').removeClass('ui-state-disabled');
		$('div#locations-menu li#cm-deleteLocation').removeClass('ui-state-disabled');
		$('div#locations-menu li#cm-objectLocation').removeClass('ui-state-disabled');
	}
	
	overlay_menu.removeClass('hidden').css({top: y+"px", left: x+"px"}) // (x-overlay_menu.width())+"px"});
}

//
//
//

function doSetPropertiesLocation(name,width,height) {
	name=name.trim();
	if ( name=='' ) {
		understandDialog('Information','Location name can not by empty','',500);
		return false;
	}
	if ( (width<32 || width>256) || (height<8 || height>256) ) {
		understandDialog('Information','Map size is incorrect','The map dimensions must be in the range:<br>32 <width <256; 8 <height <256');
		return false;
	}

	locationList[currentLocation].name=name;
	
	var	oldWidth=locationList[currentLocation].width,
		oldHeight=locationList[currentLocation].height;

	if (width!=oldWidth || height!=oldHeight) {
		var newData=[],
			oldAdr,newAdr;
		
	// copy current map data to new map	
		for (y=0;y<height;y++) {
			oldAdr=(y*oldWidth);
			newAdr=(y*width);
			if (y<oldHeight) {
				for (x=0;x<width;x++) {
					if (x<oldWidth) {
						newData[newAdr]=dataMap[oldAdr];
						oldAdr++; newAdr++;
					} else {
						newData[newAdr]=0;
						newAdr++;
					}
				}
			} else {
				for (x=0;x<width;x++) {
					newData[newAdr]=0;
					newAdr++;
				}
			}
		}
		mapData=newData;
		
	// set new information for current location 
		mapWidth=width;
		mapHeight=width;
		locationList[currentLocation].width=width;
		locationList[currentLocation].height=height;
		locationList[currentLocation].dataMap=newData;

		setMapCanvas(width,height);
		drawLocation(currentLocation);
	}
	
	makeLocationIndex();
	
	return true;
}

function propertiesLocationDialog() {
	var dlgName="dlg-propertiesLocation",
		dlgId="#"+dlgName+" ";
		
	$('<div id="'+dlgName+'" title="Location properties"/>')
	.html('<label for="currentLocationName" class="block">Location name</label> \
		   <input id="currentLocationName" type="text" value="" class="ui-inputline ui-widget ui-corner-all full-width"> \
		   <fieldset class="text-center"><legend>Map size:</legend> \
			   <fieldset><legend>Width</legend><input id="currentLocationWidth" type="number" value="" min="32" max="256" class="ui-inputline ui-widget ui-corner-all width-100p"></fieldset> \
			   <fieldset><legend>Height</legend><input id="currentLocationHeight" type="number" value="" min="8" max="256" class="ui-inputline ui-widget ui-corner-all width-100p"></fieldset> \
		   </fieldset> \
	')
	.dialog({
		width: 330,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Close it": function() { $(this).dialog('close'); },
			"I changed, save": function() { 
				var name=$(dlgId+'#currentLocationName').val();
				var width=$(dlgId+'#currentLocationWidth').val();
				var height=$(dlgId+'#currentLocationHeight').val();
				if ( doSetPropertiesLocation(name,width,height) ) {
					$(this).dialog('destroy');
				}
			}
		},
		create:function() {
			var location=locationList[currentLocation];
			$(dlgId+'#currentLocationName').val(location.name);
			$(dlgId+'#currentLocationWidth').val(location.width);
			$(dlgId+'#currentLocationHeight').val(location.height);
		},
		close: function () {
			$(this).dialog('destroy');
		}
	});
}

//
//
//

function initLocationsDialog() {
	var dlg=$('#dlg-locations');
	let windowCfg=getWindowConfig(dlg,{
		            position:{my:"left bottom", at:"left+20 bottom", of: "#viewport"},
	                width:350,
					height:200});
	dlg.dialog({
		appendTo: '#viewport',
		autoOpen: false,
		position: windowCfg.position,
		width: windowCfg.width,
		height: windowCfg.height,
		resizable:true,
		closeOnEscape: false,
		show: {effect: "fade", duration:200},
		hide: {effect: "fade", duration:200},
		open: 	function() {
			// begin issue #46
			let dockPanel=getWindowConfigProperty($(this),'dockPanel');
			if ( dockPanel ) {
				dockDialog(dlg,dockPanel);
			}
			// end issue #46
		    saveWindowConfig(this);
		},
		beforeClose: function() {
			let attachedDock=checkDockAttach($(this));
			if ( attachedDock!==undefined ) undockDialog($(this),attachedDock);
		},
		close:	function() { saveWindowConfig(this); },
		resize: function() {
            $(this).niceScroll().resize();
		},
		dragStop: function() { saveWindowConfig(this); },
		resizeStop: function() { saveWindowConfig(this); },
		dialogClass: 'dlg-locations-menu'
	});

    $(dlg).niceScroll();
	var dlgMenu=$('<button id="btn-locationMenu" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-menu" type="button" role="button" title="Options"/>')
	.html('<span class="ui-button-icon-primary ui-icon ui-icon-bars"></span><span class="ui-button-text">Options</span>')
    dlgMenu.on('click',menuLocationsDialog)

	$(".dlg-locations-menu")
	.children(".ui-dialog-titlebar")
	.prepend(dlgMenu);

	$('div#locations-menu > ul').menu({
		select:()=>{ overlay.remove(); overlay_menu.addClass('hidden'); }
	});
}
