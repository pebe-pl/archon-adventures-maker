var objectsList=[];

function showObjectLocationDialog() {
    $('#dlg-objectLocation').dialog('open')
	setMouseMode("object");

	analysisLocation(currentLocation);
}

//
//
//

function analysisLocation(locationID) {
	var location=locationList[locationID];
	var olist=$('#dlg-objectLocation ul#list');
	olist.empty();
	selectCancel=true;
	setMouseMode("object");
	animMapSelect();

	if (location===undefined) {
		var info=$('<div class="ui-state-error text-center"/>').html('location not exist :|');
		olist.append(info);
		return false;
	}

    console.groupCollapsed('Location #'+locationID+' analysis...');
	var prgs;

	var dlg=$('<div id="dlg-analysis" title="Analysis..."/>')
		.html('<div id="progress"></div>')
		.dialog({
			width:300,
			modal:true,
			autoopen:true,
			resizable:false,
			create:function() {
				prgs=$('#dlg-analysis #progress').progressbar({value:false});
			},
			close:function() {
				prgs.progressbar('destroy');
				$(this).dialog('destroy');
			}
		});
	var cntExistObj=0;
	var nobjID=0;

    console.log('Preparation of existing objects for scanning...')
	for (objID in objectsList) {
		var obj=objectsList[objID];
		if (obj!=null && obj.locationID==locationID) {
			obj.exist=false; cntExistObj++;
		}
	}
	if ( cntExistObj!=0 )
	    console.log('There are '+cntExistObj+' object(s) in the location')
	else
	    console.log('No objects found in locations')

    var cntScandObj=0;
	console.groupCollapsed('Location scanning for objects...')
	// Whole location scan:
	for (y=0;y<location.height;y++) {
		for (x=0;x<location.width;x++) {
			var adr=x+y*location.width;
			var ch=location.dataMap[adr];
			if (ch==0) continue;
			
			//
			for (tileName in tiles) {
				var tile=tiles[tileName];
				if (tile.active!==undefined && tile.active) {

					var iadr=0; correctChars=0;
					for (yy=0;yy<tile.height;yy++) {
						for (xx=0;xx<tile.width;xx++) {
							var adr=(x+xx)+(y+yy)*location.width,
								ch=location.dataMap[adr],
								ich=tile.data[iadr];
							
							if (ch==ich) {
								correctChars++;
								iadr++;
							}
						}
					}
					
					// if all characters are the same...
					if ( correctChars==(tile.height*tile.width) ) {
						// ...prepare a new object
						var nobjType=tileName.slice(0,tileName.indexOf("_"));
                        console.log('Found object '+nobjType+':');

						nobj={"exist":true, "type":nobjType, "name":"", tile:tileName, "x":x, "y":y,data:{}};
						if (location.objects===undefined) {
							location.objects=[];
						}

                        // check, if the newly created object already exists in the objectList...
						var foundObj=false;
						for (objID in objectsList) {
							obj=objectsList[objID]
							if (obj!=null) {
								if (obj.type==nobj.type && obj.x==nobj.x && obj.y==nobj.y) {
									obj.exist=true;
									foundObj=true;
									break;
								}
							}
						}

						// if the object does not exist...
						if (!foundObj) {
							console.log("Adding object '"+nobjType+"'")
							nobjID=objectsList.push(nobj); //... add it to objectList
							nobjID--;

							location.objects.push(nobjID); //... add it to location

							nobj.ID=nobjID;
							nobj.locationID=locationID;
							nobj.name=tileName.slice(tileName.indexOf("_")+1);
							cntScandObj++;
						} else {
							console.log("Object already exists")
						}
						break;
					}
				}
			}
		}
		prgs.progressbar('option',{value:(y/location.height)*100});
	}
	console.groupEnd()

    if ( cntScandObj!=0 ) 
        console.log(cntScandObj+' new object(s) were identified')
    else
        console.log('No new object found')

    cntUnusedObj=0;
    console.log('Delete unused objects...');
	for (objID in objectsList) {
		var obj=objectsList[objID];
		if (obj!=null && obj.locationID==locationID && !obj.exist) {
			delete objectsList[objID]
			cntUnusedObj++
		}
	}

    if ( cntUnusedObj!=0 ) 
        console.log(cntUnusedObj+' unused objects have been removed')
    else
        console.log('There were no objects to delete')

    var checkObject=function(objID) {
        
		objFound=false;
        console.log('Object #'+objectsList[objID].ID+'('+objectsList[objID].name+') ...')
        
    	for (var id in objectsList) {
    		var obj=objectsList[id];
			if (obj.type!="item" && obj.type!="weapons") {
				itemType=obj.type;
			} else {
				var itemType=obj.tile;
				itemType=itemType.slice(5);
				if ( itemType.indexOf('_')!=-1 ) itemType=itemType.slice(0,itemType.indexOf('_'));
			}

			if (obj.data!==undefined &&
				objTiles[itemType]!==undefined &&
				objTiles[itemType].lists!==undefined ) {
				for (listID in objTiles[itemType].lists) {
					listName=objTiles[itemType].lists[listID];
					for (listItemID in obj.data[listName]) {
						iobjID=obj.data[listName][listItemID];
						if ( iobjID==objID ) {
        					console.log("Relation in "+listName+' in object #'+obj.ID+'('+obj.name+')');
							objFound=true; break;
						}
					}
					if (objFound) break;
				}
			}
            if (objFound) break;
    	}
    	return objFound;
    }
    console.groupCollapsed('Relationships analisys...')
    cntUnusedObj=0;
	for (var id in objectsList) {
		if ( objectsList[id].exist ) continue;

        if ( !checkObject(objectsList[id].ID) ) {
            console.warn('...do not have relations. Deleted')
        	delete objectsList[id];
        	cntUnusedObj++
        }
	}
	console.groupEnd();
    if ( cntUnusedObj!=0 ) 
        console.log(cntUnusedObj+' unrelated objects have been removed')
    else
        console.log('All objects have correct relations :)')
    console.groupEnd('Scanning completed')
	makeObjectsIndexList(locationID);
	
	dlg.dialog('close');

	if (location.objects===undefined) {
		var info=$('<div class="text-center ui-state-error"/>').html('no object found :/');
		olist.append(info);
		return false;
	}
	
	return true;
}

//
// events
//

function mouseDown_Object(event) {
	var mx=event.layerX,
	my=event.layerY,
	cx=Math.floor(mx/charWidth),
	cy=Math.floor(my/charHeight);

	for (objID in objectsList) {
		obj=objectsList[objID];
		if (obj==null) continue;
        if (obj.locationID!==null && obj.locationID==currentLocation) {
			var beginX=obj.x, beginY=obj.y,
				endX=beginX+tiles[obj.tile].width-1,
				endY=beginY+tiles[obj.tile].height-1;
				
			if ((cx>=beginX && cx<=endX) &&
				(cy>=beginY && cy<=endY)) {
				doObjectSelect(objID, true);
				break;
		    }
        }
	}
}

function doObjectSelect(objID,scrollTo=false) {
	var	obj=objectsList[objID];
		
	$('#dlg-objectLocation ul#list li.select').removeClass('select');
	$('#dlg-objectLocation ul#list li').each(function(id,el) {
		var ID=$(this).data('objid');
		if (ID==objID) {
			$(this).addClass('select');
			if ( scrollTo ) {
				var $c=$('#dlg-objectLocation');
				$c.stop().scrollTo($(this),{duration:500});
			}
		}
	});
		
	selectCancel=true;
	setMouseMode("object");
	animMapSelect();

	selectStartX=obj.x;
	selectStartY=obj.y;
	selectWidth=tiles[obj.tile].width;
	selectHeight=tiles[obj.tile].height;
	
	animMapSelect();
}

// function for the event of clicking on the 'Objects in location' list item
function objectSelect(e) {
	var o=$(e.currentTarget),
		objID=o.data('objid');
		
	doObjectSelect(objID);
}

// function for double-clicking on the 'Objects in location' list item
function objectEdit(e) {
	var o=$(e.currentTarget),
		objID=o.data('objid'),
		locationID=o.data('locationid');
		
	if ( locationList[locationID]===undefined ) {
		understandDialog('Attention','Unable to identify location.','Check if you have chosen a location.',350);
		return;
	}
	doObjectEdit({obj:objectsList[objID]});
}

// the function of creating a list of objects in the current location
function makeObjectsIndexList(locationID) {
	var olist=$('#dlg-objectLocation ul#list');
	olist.empty();
    
	for (objID in objectsList) {
		var obj=objectsList[objID];
		if ( obj===null ) continue;
        if ( obj.locationID==locationID ) {
			var item=$('<li class="item" data-locationid='+obj.locationID+' data-objid='+obj.ID+'/>')
				.html('<span class="field objectID">'+obj.ID+'</span><span class="field objectName">'+obj.name+'</span><span class="field objectType">'+obj.type+'</span>');
			item.on('click',objectSelect);
			item.on('dblclick',objectEdit);
			olist.append(item);
        }
	}
	$('#dlg-objectLocation').niceScroll().resize();
}

//
//
//

function makeObject(params) {
	var objType=params.objType;

    var oType=objType.slice(0,objType.indexOf('_')),
		oName=objType.slice(objType.indexOf('_')+1);
		
	var nobj={"exist":false, "type":oType, "name":oName, locationID:null, tile:objType, "x":null, "y":null, data:{}};
	if ( params.autoedit!==undefined&& params.autoedit) 
		doObjectEdit({
			obj:nobj,
			modal:true,
			onBeforeRemeber:function(obj) {
				nobjID=objectsList.push(obj); nobjID--;
				obj.ID=nobjID;
				obj.toDelete=true;
			},
			onAfterRemeber:function(obj) {
				if ( params.onMake!==undefined ) params.onMake(obj);
			}
		});
	return nobj;
}

//
//
//

function updateInternalListItem(item,obj) {
	$(item).find('span.objID').html(obj.ID);
	$(item).find('span.name').html(obj.name);
}

function createInternalListItem(params) {
	var list=params.list;
	var	obj=params.obj;
	if (obj==null) return false;

	var objID=obj.ID;

	if (params.field!==undefined) {
		console.log('create field "'+params.field+'"');
    	var item=$('<li id="object_data_'+params.field+'[]" data-objID="'+objID+'" class="item objField"/>')
    	.html('<span class="objID"></span><span class="name"></span>');
	} else {
        var item=$('<li data-objID="'+objID+'" class="item"/>')
        .html('<span class="objID"></span><span class="name"></span>');
	}

	updateInternalListItem(item,obj);

	var but_delete=$('<span class="button ui-icon ui-icon-delete"/>');
	but_delete.click(function() {
		var objID=$(this).parent().data('objid');
        if ( objectsList[objID].locationID==null ) delete objectsList[objID];
		$(this).parent().remove();
	});
	
	if (params.editable!==undefined && params.editable) {
		item.dblclick(function() {
			var objID=$(this).data('objid');
			doObjectEdit({
				obj:objectsList[objID],
				modal:true,
				onAfterRemeber:function(){
				    updateInternalListItem(item,obj);
			    }
		    });
	    })
    }
	item.append(but_delete);
	list.append(item);
	return item;
}

//
//
//
function dialog_makeObject(onAdd) {
	var selectedItem="";
	var dlg=$('<div id="dlg_makeObject" title="Select item..."/>')
	.html('<ul class="selectList">\
			<li class="label">- Items</li>\
			<li class="item" id="item_scroll">Scroll</li>\
			<li class="item" id="item_elixir">Elixir</li>\
			<li class="item" id="item_key">Key</li>\
			<li class="item" id="item_gold">Gold</li>\
			<li class="item" id="item_skull">Skull</li>\
			<li class="label">- Weapons</li>\
			<li class="item" id="weapon_shieldsmall">Small shield</li>\
			<li class="item" id="weapon_shieldbig">Big shield</li>\
			<li class="item" id="weapon_arrows">Arrows</li>\
			<li class="item" id="weapon_spear">Spear</li>\
			<li class="item" id="weapon_axe">Axe</li>\
			<li class="item" id="weapon_sword">Sword</li>\
			<li class="item" id="weapon_bow">Bow</li>\
			<li class="label">- Monsters</li>\
			<li class="item" id="monster_Banshee">Banshee</li>\
			<li class="item" id="monster_Saurian">Saurian</li>\
			<li class="item" id="monster_Troll">Troll</li>\
			<li class="item" id="monster_Goblin">Goblin</li>\
			<li class="item" id="monster_Sorceress">Sorceress</li>\
			<li class="item" id="monster_Basilisk">Basilisk</li>\
			<li class="item" id="monster_Manticore">Manticore</li>\
			<li class="item" id="monster_Shapeshifter">Shapeshifter</li>\
		</ul>');
	$(dlg).dialog({
		autoopen:true,
		modal:true,
		resizable:false,
		width:350,
		height:500,
		buttons:{
			'Get out of here':function() { $(this).dialog('close'); },
			'Take this':function() { onAdd(selectedItem); $(this).dialog('close'); }
		},
		create:function() {
			$(dlg).find('li.item')
			.click(function(e) {
				selectedItem=e.currentTarget.id;
				$(dlg).find('li.item.select').removeClass('select');
				$(dlg).find('li#'+selectedItem+'.item').addClass('select');
			})
			.dblclick(function(e) {
				$(dlg).dialog('close');
				onAdd(selectedItem);
			});
		},
		close:function() {
			$(this).dialog('destroy');
		}
	});
}

//

function dialog_choiceObject(params) {
	var selectedLocationID="",
		selectedObjID="";

	var dlg=$('<div id="dlg_choiceObject" title="Choice object..."/>')
	.html('<ul class="selectList"></ul>');
    var doChoice=function() {
		var selectedItems=$(dlg).find('li.item.select').each(function(i,o) {
			selectedLocationID=o.dataset['locationid'];
			selectedObjID=o.dataset['objid'];
			var nobj=objectsList[selectedObjID]
			if ( typeof params.onChoice==="function" ) params.onChoice(nobj);		
		})
    }

	$(dlg).dialog({
		autoopen:true,
		modal:true,
		resizable:false,
		width:350,
		height:500,
		buttons:{
			'Get out of here':function() { $(this).dialog('close'); },
			'Take selection':function() {
				doChoice();
				$(this).dialog('close'); 
			}
		},
		create:function() {
			var list=$(dlg).find('ul');

			var getObjectList=function(locationID,filters) {
				for (objID in objectsList) {
					var obj=objectsList[objID];
					if (obj!=null) {
						if (filters!==undefined) {
							var filterMatch=false
							for (filterType in filters) {
								if ( obj.type==filters[filterType] ) {
									filterMatch=true; break;
								}
							}
						} else
							var filterMatch=true;
						if ( !filterMatch ) continue;

						if (obj.locationID==locationID) {
							var item=$('<li id="'+locationID+'-'+objID+'" class="item" data-locationID="'+locationID+'" data-objID="'+objID+'"/>').html(obj.name);
							list.append(item);
							item.click(function(e) {
								selectedLocationID=e.currentTarget.dataset['locationid'];
								selectedObjID=e.currentTarget.dataset['objid'];
								if ( !e.ctrlKey ) {
									$(dlg).find('li.item.select').removeClass('select');	
								}
								$(dlg).find('li#'+selectedLocationID+'-'+selectedObjID+'.item').addClass('select');
							})
							.dblclick(function(e) {
								doChoice();
								$(dlg).dialog('close');
							});
						}
					}
				}
			}

			for (locationID in locationList) {
				var location=locationList[locationID];
				var item=$('<li class="label"/>').html(location.name);
				list.append(item);
				getObjectList(locationID,params.filters)
			}
			var item=$('<li class="label"/>').html('Objects without assigned location');
			list.append(item);
			getObjectList(null,params.filters)
		},
		close:function() {
			$(this).dialog('destroy');
		}
	});
}

//
//
//

function applyChange_ObjectEdit(dlg,obj) {
	console.log('Apply object #'+obj.ID+' parameters:')

	var	dataObj=makeObjectData(dlg,false);
	console.log('Storing data in object...');
	for (dataField in dataObj) {
		obj[dataField]=dataObj[dataField];
	}
	makeObjectsIndexList(currentLocation);
}

function makeObjectData(dlg,isPrototype) {
	var	dataObj={};
	if ( isPrototype ) 
	    console.log('- making prototype data...')
	else
	    console.log('- making object data...')

	$(dlg).find('.objField').each(function(id,el) {
		var eid=el.id;
		if (eid.slice(0,7)=="object_") { // check, if the filed is a general definition of the object...
			if (eid.slice(7,12)=="data_") { // check, if the field if a detailed definition of the object...
				var fieldName=eid.slice(12);
				if ( fieldName.slice(-2)=='[]' ) {
//					console.log('- storing data field in array "'+fieldName+'"')
					fieldName=eid.slice(12,-2);
					if (dataObj.data===undefined) dataObj.data={};
					if (dataObj.data[fieldName]===undefined) dataObj.data[fieldName]=[];
					var objID=el.dataset['objid'];
					if ( !isPrototype ) delete objectsList[objID].toDelete;
					dataObj.data[fieldName].push(objID);
				} else {
//					console.log('- storing data field "'+fieldName+'"')
					if ( el.value!="" ) {
						if (dataObj.data===undefined) dataObj.data={};
						dataObj.data[fieldName]=el.value;
					}
				}
			} else {
				var fieldName=eid.slice(7);
//				console.log('- storing base field "'+fieldName+'"')
				dataObj[fieldName]=el.value;
			}
		}
	});
//	console.log(dataObj);
    return dataObj;
}

function checkObjectData(dlg,dataObject) {
	var dataPrototype=makeObjectData(dlg,true);
	nodeLevel=0;
    var checkNode=function(dataNodePrototype,dataNodeObject,nodeName="root") {
		isSame=true;
		nodeLevel++;
        console.groupCollapsed("Checkin node '"+nodeName+"' (level "+nodeLevel+")");
		for(dataField in dataNodePrototype) {
			objectType=typeof dataNodeObject[dataField];
			prototypeType=typeof dataNodePrototype[dataField];
			if (objectType!=="undefined") {
    			console.log('Field in object "'+dataField+'" of type "'+objectType+'"');
			} else {
    			console.warn('Field in object "'+dataField+'" is undefined!');
			}
			if (prototypeType!=="undefined") {
    			console.log('Field in prototype "'+dataField+'" of type "'+prototypeType+'"');
			} else {
    			console.warn('Field in object "'+dataField+'" is undefined!');
			}
            if ( objectType==prototypeType ) {
            	console.log('Types is the same, and his values is:')
				if ( objectType=="string" ) {
					if ( dataNodeObject[dataField]==dataNodePrototype[dataField] ) {
						console.log('Same')
					} else {
						console.warn('Different.')
						isSame=false;
					}
					continue;
				}
				if ( objectType=="object" ) {
					isSame=checkNode(dataNodePrototype[dataField],dataNodeObject[dataField],dataField)
				}
            } else {
            	console.warn('Types are differents!');
            	isSame=false;
            }
			if ( !isSame ) break;
		}
		if ( isSame ) {
			console.log("Node '"+nodeName+"' is the same")
		} else {
			console.warn("Node '"+nodeName+"' is different")
		}
		nodeLevel--;
    	console.groupEnd();
		return isSame;
	}
	return checkNode(dataPrototype,dataObject);
}
//

function doObjectEdit(params) {
	var obj=params.obj;
	if (obj===undefined) {
		console.log('Object #'+objID+' is not defined');
		return;
	}
	if (params.modal!==undefined) modal=params.modal; else modal=false;
    
	var dlg=$('div#object-edit-'+obj.ID);
	if  ( dlg.length==0 ) {
		if ( obj.ID!==undefined )
		    var dlgTitle='Object ID#'+obj.ID+' '+obj.type+' properties'
		else
		    var dlgTitle='New object '+obj.type+' properties';

		var dlg=$('<div id="object-edit-'+obj.ID+'" title="'+dlgTitle+'"/>')
			.html('\
			<label for="object_name">Object name:</label>\
			<input id="object_name" type="text" maxlength=32 value="'+obj.name+'" class="objField ui-inputline ui-widget ui-corner-all full-width">\
			<div id="object_parameters"></div>\
			');

		var res=false, closePrompt=true, applyChange=false;

		if (obj.type!="item" && obj.type!="weapons") {
			itemType=obj.type;
		} else {
			var itemType=obj.tile;
			itemType=itemType.slice(5);
			if ( itemType.indexOf('_')!=-1 ) itemType=itemType.slice(0,itemType.indexOf('_'));
		}
		
		if ( objTiles[itemType]!==undefined) {
			if ( objTiles[itemType].dlgWidth!==undefined )
				dlgWidth=objTiles[itemType].dlgWidth
			else
				dlgWidth=350;
		}

		dlg.dialog({
				width:dlgWidth,
				autoopen:false,
				modal:modal,
				resizable:false,
				buttons:{
					"Remember this":function() {
						if (params.onBeforeRemeber!==undefined) params.onBeforeRemeber(obj);
						applyChange_ObjectEdit(dlg,obj);
						if (params.onAfterRemeber!==undefined) params.onAfterRemeber(obj);
						applyChange=true;
						$(this).dialog('close');
					}
				},
				create: function() {
					var container=$(dlg).find('#object_parameters');
					if (obj.type!="item" && obj.type!="weapons") {
						itemType=obj.type;
					} else {
						var itemType=obj.tile;
						itemType=itemType.slice(5);
                        if ( itemType.indexOf('_')!=-1 ) itemType=itemType.slice(0,itemType.indexOf('_'));
					}
					if ( objTiles[itemType]!==undefined) {
						$(container).html(objTiles[itemType].HTML);
						if ( typeof objTiles[itemType].make=="function" ) objTiles[itemType].make(dlg,obj);
					} else {
						$(container).html('<div class="ui-state-error">Not implemented yet :(</div>');
					}
				},
				beforeClose:function() {
					if ( !checkObjectData(dlg,obj) && closePrompt) {
						$('<div id="dlg-objectChanged" title="Confirm"/>')
						.html('<h2>The object has been changed</h2>\
						<p>Are you sure you want to stop the changes?</p>')
						.dialog({
							autoopen:true,
							width:400,
							modal:true,
							resizable:false,
							buttons:{
								"I'm sure":function() { 
								    closePrompt=false;
								    $(this).dialog('close');
								    $(dlg).dialog('close');
								},
								"I changed my mind":function() {
									$(this).dialog('close');
								}
							}
						})
						return false;
					} else
                        return true;
				},
				close:function() {
					if ( !applyChange ) {
						for (objID in objectsList) {
							if ( objectsList[objID].toDelete!==undefined ) {
								console.log('The object #'+objID+' was created but was not approved by the user.');
								console.warn('The object will be removed from the list of objects');
								delete objectsList[objID];
							}
						}
				    }
					$(dlg).find('select').selectmenu('destroy');
					$(dlg).find('div.tabs').tabs('destroy');
					$(dlg).dialog('destroy');
				},
				show: {effect: "scale", duration:200},
				hide: {effect: "scale", duration:200},
			});

		dlg.dialog('open')
	} else {
		dlg.dialog('moveToTop');
	}
}

//
// main dialog window of location objects
//

function initObjectLocation() {
	var dlg=$('#dlg-objectLocation');
	let windowCfg=getWindowConfig(dlg,{
		            position:{my:"left top", at:"left+20 top", of: "#viewport"},
	                width:350,
					height:450});

	dlg.dialog({
		appendTo: '#viewport',
		autoOpen:false,
		position: windowCfg.position,
		width: windowCfg.width,
		height: windowCfg.height,
		resizable:true,
		modal:false,
		closeOnEscape: false,
		show: {effect: "fade", duration:200},
		hide: {effect: "fade", duration:200},
		create:function(){ },
		focus:function() { setMouseMode("object"); },
		open:function() {
			// begin issue #46
			let dockPanel=getWindowConfigProperty($(this),'dockPanel');
			if ( dockPanel ) {
				dockDialog(dlg,dockPanel);
			}
			// end issue #46
			saveWindowConfig(this); 
	    },
		beforeClose: function() {
			let attachedDock=checkDockAttach($(this));
			if ( attachedDock!==undefined ) undockDialog($(this),attachedDock);
		},
		close:function() {
			setMouseMode('paint');
			saveWindowConfig(this);
//			$(this).dialog('destroy');
		},
		resize: function() {
            $(this).niceScroll().resize();
		},
		dragStop: function() { saveWindowConfig(this); },
		resizeStop: function() { saveWindowConfig(this); },
		dialogClass: 'dlg-objects-button'
	});

    dlg.niceScroll();
	var dlgMenu=$('<button id="btn-objectRefresh" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-menu" type="button" role="button" title="Refresh object list..."/>')
	.html('<span class="ui-button-icon-primary ui-icon ui-icon-refresh"></span><span class="ui-button-text">Refresh</span>')
    dlgMenu.on('click',function() {
    	analysisLocation(currentLocation)
    })

	$(".dlg-objects-button")
	.children(".ui-dialog-titlebar")
	.prepend(dlgMenu);

}