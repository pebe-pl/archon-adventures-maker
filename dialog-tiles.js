var currentTileGroup="tile",
	currentTile="clear",
	currentPreset="default";

function showTilesDialog() {
	let dlg=$('#dlg-Tiles')
   	dlg.dialog('open');
}

//
//
//

function changeTilesGroup(e, ui) {
	var item=ui.item.value;
	$('#dlg-Tiles div.tiles-group').addClass('hidden');
	$('#dlg-Tiles div.tiles-group.always-visible').removeClass('hidden');
	$('#dlg-Tiles div#tiles-group--'+item).removeClass('hidden');
    $('#dlg-Tiles').niceScroll().resize();
}

function makeTile(tilesSet,group,name,width,height,data) {
	for (i=0;i<name.length;i++) {
		if (name[i]==" ") { name[i]="_"; }
	}

	var o=$('#dlg-Tiles div#tiles-group--'+group);
	if (o.length==0) {
		var o=$('<div id="tiles-group--'+group+'"/>').addClass('tiles-group hidden');
		$('#dlg-Tiles').append(o);
		var o=$('<option id="'+tilesSet+'" class="'+(tilesSet!=-1?"tilesSet":"")+'" value="'+group+'"/>').html(group);
		$('#dlg-Tiles select#tiles-group-select').append(o);
		$('#dlg-Tiles #tiles-group-select').selectmenu('refresh');
	}

	var o=$('#dlg-Tiles div#tiles-group--'+group+' #tile_'+name);
	if ( o.length>0 ) return;

	var canvas=$('<canvas id="tileCanvas" width="'+(width*charWidth)+'" height="'+(height*charHeight)+'"/>');
	var ctx=canvas[0].getContext('2d');
	var i=0, oCurrentChars=currentChars;
	if (tilesSet==-1)
		currentChars=0;
	else
		currentChars=tilesSet;

	for(y=0;y<height;y++) {
		for(x=0;x<width;x++) {
			drawChar(x,y,data[i],ctx);
			i++;
		}
	}
	CurrentChars=oCurrentChars;
	var content=$('<div id="tile_'+name+'" title="Tile size '+width+'x'+height+'" class="tiles"/>').html('<span>'+name+'</span><div class="key hidden"></div>').append(canvas);

	content.on('click',selectTile);
	content.on('contextmenu',showContextMenuTile);

	$('#dlg-Tiles div#tiles-group--'+group).append(content);
	assignShortcutKey(name,tiles[name].key);
    $('#dlg-Tiles').niceScroll().resize();
	return true;
}

function assignShortcutKey(tile,key) {
	if (key===undefined) return false;
	key=key.trim();
	tiles[tile].key=key;
	var o=$('#dlg-Tiles #tile_'+tile+' > div.key');
	if (o.length>0) {
		o.html(key);
		if ( key=="" ) 
			o.addClass('hidden')
		else
			o.removeClass('hidden');
		return true;
	}
	return false;
}

function changeTilesSet(tilesSet) {
	$('#dlg-Tiles div.tiles-group').addClass('hidden');
	if (tilesSet!=-1)
		$('#dlg-Tiles div.tiles-group.always-visible').removeClass('hidden');

	if (tilesSet!=-1) {
		var o=$('#dlg-Tiles #tiles-group-select option#'+tilesSet+'.tilesSet'),
			v=o.val();
		$('#dlg-Tiles #tiles-group-select option').prop('disabled',false);
		$('#dlg-Tiles #tiles-group-select option.tilesSet').prop('disabled',true);
		$('#dlg-Tiles #tiles-group-select option.empty').prop('disabled',true);
	} else {
		$('#dlg-Tiles #tiles-group-select option.empty').prop('selected',true).prop('hidden',true);
		$('#dlg-Tiles #tiles-group-select option').prop('disabled',true);
	}
	$('#dlg-mapCharacters canvas.tilesSet').addClass('hidden');
	if (tilesSet!=-1)
		$('#dlg-mapCharacters canvas#mapCharacterCanvas'+tilesSet+'.tilesSet').removeClass('hidden');

	$('#dlg-Tiles #tiles-group--'+v).removeClass('hidden');
	$('#tile_'+currentTile).removeClass('select');

	if (tilesSet!=-1) {
		o.prop('disabled',false).prop('selected',true);
		currentTile="clear";
		currentTileGroup="tile";
		cursorWidth=tiles[currentTile].width;
		cursorHeight=tiles[currentTile].height;
		$('#tile_'+currentTile).addClass('select');
//		$('#dlg-mapCharacters #currentChars').val(tilesSetName[currentChars]);
	} else {
//		$('#dlg-mapCharacters #currentChars').val('');
	}
	$('#dlg-Tiles #tiles-group-select').selectmenu('refresh');
    $('#dlg-Tiles').niceScroll().resize();
}

function selectTile(e) {
	selectCancel=true;

	if ( drawMode!='paint' ) {
		setMouseMode('paint');
	}

	if (currentTileGroup=="char") {
		var y=currentTile >> 5;
		var x=currentTile % 32;
		var i=y*32+x;
		drawChar(x,y,i,mapChar[currentChars]);
	}

	var tile_id=e.currentTarget.id;
	var tile_name=tile_id.substring(5);

	if (grid!==undefined) {
		grid.strokeStyle='#000000';
		draw_Cursor(ocx,ocy);
	}

	setPaintMethod('point');
	$('#dlg-Tiles div.tiles.select').removeClass('select');
	currentTileGroup=tile_id.substring(0,4);
	currentTile=tile_name;
	cursorWidth=tiles[currentTile].width;
	cursorHeight=tiles[currentTile].height;
	$('#tile_'+currentTile).addClass('select');
}

//
//
//

function showContextMenuTile(e) {
	e.preventDefault();
	selectTile(e);
	overlay_menu=$('#mapTiles-contextMenu');
	var x=e.pageX,	y=e.pageY,
		w=overlay_menu.width(),	h=overlay_menu.height(),
		cw=$('html').prop('clientWidth')-1,
		ch=$('html').prop('clientHeight');

	overlay=$('<div class="menu-overlay"/>').click(function() { 
		overlay.remove();
		overlay_menu.addClass('hidden');
	}).appendTo('body');

	if (x+w>cw) x=cw-w;
	if (y+h>ch) y=y-h;

	if (tiles[currentTile].group!='user') {
		$('#mapTiles-contextMenu li#cm_deleteTile').addClass('ui-state-disabled');
		$('#mapTiles-contextMenu li#cm_renameTile').addClass('ui-state-disabled');
	} else {
		$('#mapTiles-contextMenu li#cm_deleteTile').removeClass('ui-state-disabled');
		$('#mapTiles-contextMenu li#cm_renameTile').removeClass('ui-state-disabled');
	}			
	$("#mapTiles-contextMenu").removeClass('hidden').css({top: y+"px", left: x+"px"});
	return false;
}

//
//
//

function doSetShortcutKeyTile() {
	var selectKey=$('#dlg-shortCutKeyTile select#list_keyShortcut').val();
	console.log(selectKey);
	var current="";
	for (tile in tiles) {
		if ( tiles[tile].key!==undefined && tiles[tile].key==selectKey ) {
			current=tile;
			break;
		}
	}
	console.log(current);
	if ( current!="" && current!=currentTile ) {
		$('<div id="dlg-reassignKey" title="Confirm"/>')
		.html("<div class='text-center'><h2>Do you want to assign a shortcut key to the tile '"+currentTile+"'?</h2> \
			   <p>The shortcut key you choose is already assigned to the tile <b>'"+current+"'</b></p></div>")
		.dialog({
			width:700,
			autoopen:true,
			resizable:false,
			modal:true,
			buttons: {
				"Don't do this": function() { $(this).dialog('close'); },
				'Do it': function() { 
					assignShortcutKey(current,'');
					assignShortcutKey(currentTile,selectKey);
					$(this).dialog('close');
					$('#dlg-shortcutKeyTile').dialog('close');
				},
			},
			close: function() {
				$(this).dialog('destroy');
			}
		});;
		return false;
	}

	if (current!="") assignShortcutKey(current,'');
	assignShortcutKey(currentTile,selectKey);
	return true;
}

function doClearShortcutKeyTile() {
	if ( userConfig['infoClearShortcut'] ) {
		var dlgInfo=$('<div id="dlg-clearShortcutTile" title="Information"/>').html(' \
		<div class="text-center"> \
			<h2>You want to delete the tile shortcut key</h2> \
			<input id="infoClearShortcut" type="checkbox"><label \ for="infoClearShortcut">Never show this message</label> \
		</div> \
		').dialog({
			width: 440,
			autoopen:true,
			resizable:false,
			modal:true,
			buttons: {
				"Do it!": function() {
					assignShortcutKey(currentTile,'');
					$(this).dialog('close');
					$('#dlg-shortcutKeyTile').dialog('close');
				},
				"Don't do this": function() { $(this).dialog('close'); },
			},
			create: function() {
				$('#dlg-clearShortcutTile input#infoClearShortcut').checkboxradio();
			},
			close: function () {
				userConfig['infoNewWorld']=!$('#dlg-clearShortcutTile input#infoClearShortcut').prop('checked');
				saveConfig();
				$('#dlg-clearShortcutTile input#infoClearShortcut').checkboxradio("destroy");
				$(this).dialog('destroy');
			}
		});
	} else {
		assignShortcutKey(currentTile,'');
		return true;
	}

}

function setShortcutKeyTile(e) {
	$('<div id="dlg-shortcutKeyTile" title="Configure shortcut key for a tile"/>')
	.html('<div class="text-center"> \
		   <fieldset class="text-center"><legend>Choice a key:</legend> \
		   <select id="list_keyShortcut"></select> \
		   </filedset></div>')
	.dialog({
		width: 400,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Back": function() { $(this).dialog('close'); },
			"Clear": function() { if ( doClearShortcutKeyTile() ) { $(this).dialog('close'); } },
			"Set": function() { if ( doSetShortcutKeyTile() ) {	$(this).dialog('close'); } }
		},
		create: function() {
			var currentKey=tiles[currentTile].shortcutKey;
			if ( currentKey===undefined ) {
				$('#dlg-shortcutKeyTile #currentKey').val("not assigned");
			} else {
				$('#dlg-shortcutKeyTile #currentKey').val(currentKey);
			}
			var list=$('#dlg-shortcutKeyTile select#list_keyShortcut');
			var keys='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			for (i=0;i<keys.length;i++) {
				var current="";
				for (tile in tiles) {
					if ( tiles[tile].key!==undefined && tiles[tile].key==keys[i] ) {
						current=tile;
						break;
					}
				}
				if ( current=="" ) current="not assigned";
				var opt=$('<option value="'+keys[i]+'"/>').html(keys[i]+' - '+current);
				list.append(opt);
			}
			list.selectmenu({width:250}).selectmenu('menuWidget').addClass('list-overflow');
		},
		close: function () {
			$(this).dialog('destroy');
		}

	});
}


function keyShortcutKey(e) {
	key=(e.originalEvent.key).toUpperCase();

	for (tileName in tiles) {
		var tile=tiles[tileName];
		if (tile.key!==undefined && tile.key==key) {
			if (tile.tileSet==currentChars) {
				if (currentTileGroup=="char") {
					selectCancel=true;
					var y=currentTile >> 5;
					var x=currentTile % 32;
					var i=y*32+x;
					drawChar(x,y,i,mapChar[currentChars]);
				}
				if (grid!==undefined) {
					grid.strokeStyle='#000000';
					draw_Cursor(ocx,ocy);
				}

				$('#tile_'+currentTile).removeClass('select');
				currentTileGroup='tile';
				currentTile=tileName;
				cursorWidth=tile.width;
				cursorHeight=tile.height;
				group=tile.group;
				$('#dlg-Tiles select#tiles-group-select option[value="'+group+'"]').prop('selected',true);
				$('#dlg-Tiles select#tiles-group-select').selectmenu('refresh');
				$('#dlg-Tiles div.tiles-group').addClass('hidden');
				$('#dlg-Tiles div#tiles-group--'+group).removeClass('hidden');

				$('#tile_'+tileName).addClass('select');
			}
			break;
		}
	}
}

//
//
//

function menuTilesDialog(e) {
	var x=e.pageX, y=e.pageY;

	overlay_menu=$('div#tiles-menu');
	overlay=$('<div class="menu-overlay"/>').click(function() { 
		overlay.remove(); overlay_menu.addClass('hidden');
	}).appendTo('body');

	overlay_menu.removeClass('hidden').css({top: y+"px", left: x+"px"}) // (x-overlay_menu.width())+"px"});
}
//
//
//

function readBlobPreset(files) {
	if (!files.length) {
		return;
	}

	var dlgInfo=$('<div id="messageBox" title=""/>').html(' \
	<div class="text-center"> \
		<h2>Loading preset data...</h2> \
	</div> \
	').dialog({
		width: 400,
		autoopen:true,
		resizable:false,
		modal:true,
		close: function () {
			$(this).dialog('destroy');
		}
	});

	var file = files[0];
	var start = 0;
	var stop = file.size - 1;

	var reader = new FileReader();
	// If we use onloadend, we need to check the readyState.
	reader.onloadend = function(evt) {
		if (evt.target.readyState == FileReader.DONE) { // DONE == 2
			filedata=JSON.parse(evt.target.result);
			tiles=filedata.preset;
/* Updating the Tiles List window */
			$('#dlg-Tiles div.tiles-group').empty();
			for(set in tiles) {
				for(entry in tiles[set]) {
					if ( entry=="" ) continue;
					var tile=tiles[set][entry];
					makeTile(tile.tilesSet,tile.group,entry,tile.width,tile.height,tile.data);
				};
			}
		};
		dlgInfo.dialog('close');
	};

    var blob = file.slice(start, stop + 1);
    reader.readAsBinaryString(blob);
}

function doLoadPreset() {
	var files=$('#dlg-loadPReset #fileChoice').get(0).files;
	if (files.length==1) {
		readBlobPreset(files);
		return true;
	} else {
		understandDialog("Information", "Choice a file", "To download a file, you must first select it by clicking the 'Browse...' button", 400);
		return false;
	}
}

function loadPresetTiles() {
	var dlgInfo=$('<div id="dlg-loadPreset" title="Load tile preset"/>').html(' \
	<div> \
		<label for="presetFileName" class="block">Preset file name</label> \
		<input id="fileChoice" type="file" class="hidden"> \
		<input id="presetFileName" type="text" readonly class="ui-inputline ui-widget ui-corner-all full-width ui-state-disabled"> \
	</div> \
	').dialog({
		width: 440,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Browse...": function() {
				$('#dlg-loadPreset #fileChoice').trigger('click');
			},
			"Upload preset": function() { if ( doLoadPreset() ) { $(this).dialog('close'); } },
			"Close": function() { $(this).dialog('close'); },
		},
		create: function () {
			$('#dlg-loadPreset #fileChoice').on('change', function () {
				var files=$(this).get(0).files;
				console.log(files);
				if (files.length==1) {
					$('#dlg-loadPreset input#presetFileName').val(files[0].name);
				} else {
					$('#dlg-loadPreset input#presetFileName').val('');
				}
			});
		},
		close: function () {
			$(this).dialog('destroy');
		}
	});
	return false;
}

//
//
//

function doSavePreset() {
	var fileName=$('#dlg-savePreset #presetFileName').val().trim();
	if (fileName=="") {
		understand("Information", "File name can not be empty","",400);
		return false;
	}

	currentPreset=fileName;
	currentFileFormat=$('#dlg-savePreset #fileFormat').val();

	if (currentFileFormat=="JSON") {
		var fileContent = JSON.stringify({"version":app_version,"preset":tiles});
	} else {
		return false;
	}

	var bb = new Blob([fileContent], { type: 'text/plain' });
	var a = document.createElement('a');
	a.download = currentPreset+"."+currentFileFormat;
	a.href = window.URL.createObjectURL(bb);
	a.click();
	return true;
}

function savePresetTiles() {
	var dlgInfo=$('<div id="dlg-savePreset" title="Save tiles preset"/>').html(' \
	<div> \
		<label for="presetFileName" class="block">Preset file name</label> \
		<input id="presetFileName" type="text" value="'+currentPreset+'" class="ui-inputline ui-widget ui-corner-all full-width"> \
		<label for="fileFormat" class="block">Choice file format</label> \
		<select id="fileFormat"> \
			<option value="JSON" selected>JSON text</option> \
		</select> \
	</div> \
	').dialog({
		width: 400,
		autoopen:true,
		resizable:false,
		modal:true,
		buttons: {
			"Download file": function() { if ( doSavePreset() ) { $(this).dialog('close'); } },
			"Close": function() { $(this).dialog('close'); },
		},
		create: function () {
			$('#dlg-savePreset select#fileFormat').selectmenu();
		},
		close: function () {
			$('#dlg-savePreset select#fileFormat').selectmenu('destroy');
			$(this).dialog('destroy');
		}
	});
	return false;
}
//
//
//

//
// Prepare tiles list dialog
//

function initTilesDialog() {
	let dlg=$('#dlg-Tiles');
	let windowCfg=getWindowConfig(dlg,{
		            position:{my:"left top", at:"left+100 top+50", of: "#viewport"},
	                width:200,
					height:450});

	dlg.dialog({
		appendTo: '#viewport',
		autoOpen:false,
		position: windowCfg.position,
		width: windowCfg.width,
		height: windowCfg.height,
		resizable: true,
		closeOnEscape: false,
		show: {effect: "fade", duration:200},
		hide: {effect: "fade", duration:200},
		open: function() {
			// begin issue #46
			let dockPanel=getWindowConfigProperty($(this),'dockPanel');
			if ( dockPanel ) {
				dockDialog(dlg,dockPanel);
			}
			// end issue #46
			saveWindowConfig(this);
	    },
		beforeClose: function() {
			let attachedDock=checkDockAttach($(this));
			if ( attachedDock!==undefined ) undockDialog($(this),attachedDock);
		},
		close:	function() { saveWindowConfig(this); },
		resize: function() {
            let widthDlg = $(this).prev('.ui-dialog-titlebar').outerWidth(true);
            $(this).find('#tiles-group-select').selectmenu("option","width",widthDlg);
            $(this).niceScroll().resize();
		},
		dragStop: function() { saveWindowConfig(this); },
		resizeStop: function() { saveWindowConfig(this); },
		dialogClass: 'dlg-tiles-menu'
	});

    $(dlg).niceScroll();
	var dlgMenu=$('<button id="btn-tilesMenu" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-menu" type="button" role="button" title="Options"/>')
	.html('<span class="ui-button-icon-primary ui-icon ui-icon-bars"></span><span class="ui-button-text">Options</span>')
    dlgMenu.on('click',menuTilesDialog)

	$(".dlg-tiles-menu")
	.children(".ui-dialog-titlebar")
	.prepend(dlgMenu);

	$('div#tiles-menu > ul').menu({
		select:()=>{ overlay.remove(); overlay_menu.addClass('hidden'); }
	});
	$('#mapTiles-contextMenu > ul').menu({
		select:()=>{ overlay.remove(); overlay_menu.addClass('hidden'); }
	});
	$('#tiles-group-select').selectmenu({
		width:200,
		classes: {"ui-selectmenu-button":"fixed-groupSelect"},
		change: changeTilesGroup
	});

	for(entry in tiles) {
		if ( entry=="" ) continue;
		var tile=tiles[entry];
		makeTile(tile.tileSet,tile.group,entry,tile.width,tile.height,tile.data);
	};

	currentChars=-1;
	changeTilesSet(currentChars);

	$('#tile_clear').addClass('select');

	$(document).on('keypress', function(e) { keyShortcutKey(e); } );
}
