function dialogCreatorData() {
	var id="dlg-creatorData", dlgId="#"+id+" ";
	$('<div id="'+id+'" title="Creator data" />')
	.html('<div> \
			<p class="ui-state-error text-center">This information is <u>not required</u>,<br>if you don\'t intend to make Worlds available on the Global Worlds Service</p>\
			<fieldset class="full-width"><legend>Information about creator</legend> \
			<label for="creatorNick">Your nick:</label> \
			<input id="creatorNick" type="text" value="'+userConfig['user'].nick+'" maxlength="32" required class="ui-inputline ui-widget ui-corner-all full-width"> \
			<label for="creatorCountry">Where are you from:</label> \
			<input id="creatorCountry" type="text" value="'+userConfig['user'].country+'" maxlength="64" class="ui-inputline ui-widget ui-corner-all full-width"> \
			<label for="creatorContact">Contact:</label> \
			<input id="creatorContact" type="text" value="'+userConfig['user'].contact+'" maxlength="128" class="ui-inputline ui-widget ui-corner-all full-width"> \
			\
		   </fieldset></div>')
	.dialog({
		autoopen:true,
		width:400,
		height:500,
		resizable:false,
		modal:true,
		buttons:{
			"Leave this place":function() { $(this).dialog('close'); },
			"Keep info":function() { 
				userConfig['user'].nick=$(dlgId+' #creatorNick').val();
				userConfig['user'].country=$(dlgId+' #creatorCountry').val();
				userConfig['user'].contact=$(dlgId+' #creatorContact').val();
				saveConfig();
				$(this).dialog('close'); 
			}
		},
		close:function(){
			$(this).dialog('destroy');
		}
	});
}