var	chars=[],
	currentChars=0,
	mapCharCanvas=[],
	mapChar=[],

	och_cx, och_cy;
	
function showCharactersMapDialog() {
	$('#dlg-mapCharacters').dialog('open');
}

function mouseEventMapChar(res,e) {
	event=e.originalEvent;
	if (event.button==0) {
		var mx=event.layerX,
			my=event.layerY,
			ch_cx=Math.floor(mx/charWidth),
			ch_cy=Math.floor(my/charHeight);

		if (res=="down") {
			currentTileGroup="char";
			setMouseMode('paint');
			selectCancel=true;
		}
		
		if (res=="up") {
			var i=och_cy*32+och_cx;
			drawChar(och_cx,och_cy,i,mapChar[currentChars]);
			$('#tile_'+currentTile).removeClass('ui-state-active');
			cursorWidth=1;
			cursorHeight=1;
			currentTile=(ch_cy*32+ch_cx);
			selectCancel=false;
			animCharSelect();
		}

		if (res=="out") {
			var i=och_cy*32+och_cx;
			drawChar(och_cx,och_cy,i,mapChar[currentChars]);
		}
		
		if (res=="move") {
			if ( (ch_cx!==och_cx) || (ch_cy!==och_cy) ) {
				var i=och_cy*32+och_cx;
				drawChar(och_cx,och_cy,i,mapChar[currentChars]);
	//			mapChar.clearRect(px-1,py,charWidth+1,charHeight);

				px=ch_cx*charWidth+0.5; py=ch_cy*charHeight+0.5;	// draw new cursor
				mapChar[currentChars].lineWidth=1;
				mapChar[currentChars].strokeStyle='blue';
				mapChar[currentChars].strokeRect(px,py,charWidth-1,charHeight-1);
				och_cx=ch_cx; och_cy=ch_cy;
			}
		}
	}
}

function animCharSelect() {
	var y=currentTile >> 5;
	var x=currentTile % 32;
	
	mapChar[currentChars].strokeStyle='#000000';
	mapChar[currentChars].lineWidth=1;
	mapChar[currentChars].setLineDash([]);
	mapChar[currentChars].strokeRect((x*charWidth)+0.5,(y*charHeight)+0.5,
									 charWidth-1,charHeight-1);

	if (currentTileGroup!="char" || selectCancel) {
		selectCancel=false;
		return;
	}
	selectOffset++;
	if (selectOffset>16) selectOffset=0;
	
	mapChar[currentChars].strokeStyle='yellow';
	mapChar[currentChars].setLineDash([4,4]);
	mapChar[currentChars].lineDashOffset=-selectOffset;
	mapChar[currentChars].strokeRect((x*charWidth)+0.5,(y*charHeight)+0.5,
									  charWidth-1,charHeight-1);
	mapChar[currentChars].setLineDash([]);
	mapChar[currentChars].lineDashOffset=0;
	setTimeout(animCharSelect, 20);			
}

//
// prepare Map Character Dialog
//

function initCharacterMapDialog() {
	for (currentChars=0;currentChars<=2;currentChars++) {
		chars[currentChars]=document.getElementById('tiles-'+currentChars);
		mapCharCanvas[currentChars]=$('<canvas id="mapCharacterCanvas'+currentChars+'" width="512" height="128"/>').addClass('tilesSet hidden');
		mapChar[currentChars]=mapCharCanvas[currentChars][0].getContext('2d');
		$('div#dlg-mapCharacters').prepend(mapCharCanvas[currentChars]);

		var i=0;
		for(y=0;y<4;y++) {
			for(x=0;x<32;x++) {
				drawChar(x,y,i,mapChar[currentChars]);
				i++;
			}
		}

		mapCharCanvas[currentChars].on('mousemove', (e) => { mouseEventMapChar('move',e); });
		mapCharCanvas[currentChars].on('mouseout',  (e) => { mouseEventMapChar('out',e); });
		mapCharCanvas[currentChars].on('mousedown', (e) => { mouseEventMapChar('down',e); });
		mapCharCanvas[currentChars].on('mouseup',   (e) => { mouseEventMapChar('up',e); });
		mapCharCanvas[currentChars].on('contextmenu',  (e) => { e.preventDefault(); });
	}

	let dlg=$('#dlg-mapCharacters');
	let windowCfg=getWindowConfig(dlg,{
		            position:{my:"left bottom", at:"left+10 bottom-10", of: "#viewport"},
	                width:522,
					height:180});

	dlg.dialog({
		appendTo: '#viewport',
		autoOpen: false,
		position: windowCfg.position,
		width: windowCfg.width,
		height: windowCfg.height,
		resizable:false,
		open: function() {
		    saveWindowConfig(this); 
		},
		close: function() { saveWindowConfig(this); },
		dragStop: function() { saveWindowConfig(this); },
		resizeStop: function() { saveWindowConfig(this); },
        show: {effect: "fade", duration:300},
		hide: {effect: "fade", duration:300}
	})

	currentChars=-1;

}
