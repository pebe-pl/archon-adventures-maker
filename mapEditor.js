const 	draw_onCanvas=0,
		draw_onMap=1,
		draw_mapSource=2;
		
var		flag = false,	//	mouse flags
	dot_flag = false;	//
	
var mapCanvas, map,
	gridCanvas, grid,
	charGridCanvas, charGrid,
	
	screenWidth=32,	screenHeight=16,
	charWidth=16,	charHeight=32,
	mapWidth=0,	mapHeight=0,
	cursorWidth=1,	cursorHeight=1,
	ocx,ocy,
	selectOffset=0;	

var beginX,beginY,
	endX,endY;

var drawMode="paint";
var paintMethod="point";

var	dataMap=[];

//
//
//

function drawChar(x,y,ch,canvas) {
	if (ch<255) {
		var	cy=(ch >> 5),
			cx=(ch % 32),
			px=x*charWidth, py=y*charHeight;
			cx=cx*16; cy=cy*16;
		canvas.drawImage(chars[currentChars],cx,    cy,charWidth,charWidth,px,py   ,charWidth,charWidth);
		canvas.drawImage(chars[currentChars],cx,128+cy,charWidth,charWidth,px,py+16,charWidth,charWidth);
	} else {
		ch=ch-256;
		var	cy=(ch >> 5),
			cx=(ch % 32),
			px=x*charWidth, py=y*charHeight;
			cx=cx*16; cy=cy*16;
		canvas.drawImage(chars[2],cx,    cy,charWidth,charWidth,px,py   ,charWidth,charWidth);
		canvas.drawImage(chars[2],cx,128+cy,charWidth,charWidth,px,py+16,charWidth,charWidth);
	}
}

function drawTile(cx,cy,tile_name) {

	var i=0,ch=0;
	var tile=tiles[tile_name];
	var oCurrentChar=currentChars;
	if (tile.tileSet==-1) 
		currentChars=0
	else
		currentChars=tile.tileSet;
	for (y=0;y<tile.height;y++) {
		var adr=((y+cy)*mapWidth)+cx;
		for (x=0;x<tile.width;x++) {
			ch=tile.data[i];
			drawChar(cx+x,cy+y,ch,map);
			dataMap[adr]=ch;
			adr++; i++;
		}
	}
	currentChars=oCurrentChar;
}

function drawScreenGrid() {
	if ( userConfig['screenGrid'] ) {
		grid.beginPath();
		grid.strokeStyle='#444444';
		grid.lineWidth=1;
		for (y=1;y<=(mapHeight/screenHeight)+1;y++) {
			for (x=1;x<=(mapWidth/screenWidth);x++) {
				grid.moveTo(screenWidth*16*x+.5,0);
				grid.lineTo(screenWidth*16*x+.5,4096);
				grid.moveTo(0,screenHeight*16*y+.5);
				grid.lineTo(4096,screenHeight*16*y+.5);
			}
		}
		grid.stroke();
	} else {
	}
}

function drawFontSetGrid() {
	if ( userConfig['fontSetGrid'] ) {
		charGrid.fillStyle='#444444';
		charGrid.lineWidth=1;
		for (y=1;y<mapHeight;y++) {
			for (x=1;x<mapWidth/2;x++) {
				charGrid.fillRect(x*charWidth*2,y*charHeight,1,1);
			}
		}
	} else {
		charGrid.clearRect(0,0,4096,4096);
	}
}

//
//
//

function setMapCanvas(width,height) {
	$('div#map-content').empty();
	
	var widthInPx=width*charWidth,
		heightInPx=height*charHeight;

	mapWidth=width;
	mapHeight=height;

	mapCanvas=$('<canvas id="mapCanvas" width="'+widthInPx+'" height="'+heightInPx+'"/>');
	map=mapCanvas[0].getContext('2d');
	$('div#map-content').append(mapCanvas);
	
	charGridCanvas=$('<canvas id="charGridCanvas" width="'+widthInPx+'" height="'+heightInPx+'"/>');
	charGrid=charGridCanvas[0].getContext('2d');
	$('div#map-content').append(charGridCanvas);
	
	gridCanvas=$('<canvas id="gridCanvas" width="'+widthInPx+'" height="'+heightInPx+'"/>');
	grid=gridCanvas[0].getContext('2d');
	$('div#map-content').append(gridCanvas);

	gridCanvas.on('mousemove', function(e) { mouseEventMap('move',e); });
	gridCanvas.on('mousedown', function(e) { mouseEventMap('down',e); });
	gridCanvas.on('mouseup',   function(e) { mouseEventMap('up',e); });
	gridCanvas.on('mouseout',  function(e) { mouseEventMap('out',e); });
	gridCanvas.on('contextmenu',  function(e) { e.preventDefault(); });
	
	map.fillStyle="#000000";
	map.fillRect(0,0,widthInPx,heightInPx);

	drawFontSetGrid();
	drawScreenGrid();
}

//
//
//

function drawWall(x0, y0, x1, y1, draw=draw_onCanvas) {
	if (y1<y0) { 
		y=y0; y0=y1; y1=y;
		x=x0; x0=x1; x1=x;
	}
	
	var dx = Math.abs(x1 - x0);
	var dy = Math.abs(y1 - y0);
	var sx = (x0 < x1) ? 1 : -1;
	var sy = (y0 < y1) ? 1 : -1;
	var err = dx - dy;
	
	while(true) {
		adr=x0+(y0*mapWidth);
		if ( draw==draw_onCanvas ) drawChar(x0,y0,2,map)
		if ( draw==draw_onMap ) dataMap[adr]=2;
		if ( draw==draw_mapSource ) drawChar(x0,y0,dataMap[adr],map);

		if ((x0 === x1) && (y0 === y1)) break;
		var e2=2*err;
		if (e2 > -dy) { err-=dy; x0+=sx; }
		if (e2 < dx) {
			adr=x0+(y0*mapWidth);
			if ( draw==draw_onCanvas ) drawChar(x0,y0,30,map)
			if ( draw==draw_onMap ) dataMap[adr]=30;
			if ( draw==draw_mapSource ) drawChar(x0,y0,dataMap[adr],map);
			err+=dx; y0+=sy; 
		}
	}
}

function doDrawOnMap(x,y) {
	if ( paintMethod=="point" ) {
		if (currentTileGroup=="char") {
			drawChar(x,y,currentTile,map);
			var adr=(y*mapWidth)+x;
			dataMap[adr]=currentTile;
		} else {
			drawTile(x,y,currentTile);
		}
		return;
	}
	if ( paintMethod=="line" ) {
		if (flag) {
			if (dot_flag) {
				beginX=x; beginY=y;
				endX=x; endY=y;
			} else {
				drawWall(beginX,beginY,endX,endY,draw_mapSource);
				endX=x; endY=y;
				drawWall(beginX,beginY,endX,endY,draw_onCanvas);
			}
			
		} else {
			drawWall(beginX,beginY,endX,endY,draw_onMap);
		}
	}
	
	if ( paintMethod=="box" ) {
		if (flag) {
			if (dot_flag) {
				beginX=x; beginY=y;
				endX=x; endY=y;
			} else {
				drawWall(beginX,beginY,endX,beginY,draw_mapSource);
				drawWall(beginX,endY,endX,endY,draw_mapSource);
				drawWall(beginX,beginY,beginX,endY,draw_mapSource);
				drawWall(endX,beginY,endX,endY,draw_mapSource);
				endX=x; endY=y;
				drawWall(beginX,beginY,endX,beginY,draw_onCanvas);
				drawWall(beginX,endY,endX,endY,draw_onCanvas);
				drawWall(beginX,beginY,beginX,endY,draw_onCanvas);
				drawWall(endX,beginY,endX,endY,draw_onCanvas);
			}
			
		} else {
			drawWall(beginX,beginY,endX,beginY,draw_onMap);
			drawWall(beginX,endY,endX,endY,draw_onMap);
			drawWall(beginX,beginY,beginX,endY,draw_onMap);
			drawWall(endX,beginY,endX,endY,draw_onMap);
		}
	}
}

//
//
//

function draw_Cursor(cx,cy) {
	var px=cx*charWidth+0.5,
		py=cy*charHeight+0.5,
		pw=charWidth*cursorWidth,
		ph=charHeight*cursorHeight;
	if ( grid.strokeStyle!="#000000" ) {
		grid.strokeRect(px,py,pw-1,ph-1);
	} else {
		grid.clearRect(px-0.5,py-0.5,pw,ph);
	}
}

function draw_Selection() {
	if ( selectWidth<=0 || selectHeight<=0 ) return;
	var sx=(selectStartX*charWidth),
		sy=(selectStartY*charHeight),
		sw=(selectWidth*charWidth),
		sh=(selectHeight*charHeight);
	grid.strokeRect(sx,sy,sw-1,sh-1);
}

//
//
//
function mouseDown_Select(event) {
	var mx=event.layerX,
		my=event.layerY,
		cx=Math.floor(mx/charWidth),
		cy=Math.floor(my/charHeight);

//	grid.strokeStyle='#000000';
//	draw_Selection();
	grid.clearRect(0,0,mapWidth*charWidth,mapHeight*charHeight);
	grid.strokeStyle='blue'; grid.lineWidth=1;
	drawScreenGrid();
	
	selectStartX=cx;	selectStartY=cy;
	selectEndX=-1;		selectEndY=-1;
	selectWidth=-1;		selectHeight=-1;
	selectCancel=false;
	draw_Selection();
	
	drawMode="selecting";
	if ( !$("fieldset#clipboard-tools").hasClass('hidden') ) {
		$("fieldset#clipboard-tools").addClass('hidden');
	}
}

function mouseMove_Select(event) {
	var mx=event.layerX,
		my=event.layerY,
		cx=Math.floor(mx/charWidth),
		cy=Math.floor(my/charHeight);

	selectWidth =selectEndX-selectStartX+1;
	selectHeight=selectEndY-selectStartY+1;
	grid.clearRect(0,0,mapWidth*charWidth,mapHeight*charHeight);
	drawScreenGrid();

	if (selectEndX>=0 && selectEndX<selectStartX) selectStartX=cx;
	if (selectEndY>=0 && selectEndY<selectStartY) selectStartY=cy;

	selectEndX=cx; selectEndY=cy;
	selectWidth =selectEndX-selectStartX+1;
	selectHeight=selectEndY-selectStartY+1;
	grid.strokeStyle='blue';
	grid.lineWidth=1;
	draw_Selection();
}

function mouseUp_Select(event) {
	var mx=event.layerX,
		my=event.layerY,
		cx=Math.floor(mx/charWidth),
		cy=Math.floor(my/charHeight);

	if (selectWidth<0 || selectHeight<0) {
		drawMode="select";
	} else {
		selectWidth =selectEndX-selectStartX+1;
		selectHeight=selectEndY-selectStartY+1;
		drawMode="select";
		if (selectWidth>0 && selectHeight>0) {
			animMapSelect();
			if ( paintMethod=="clear" ) selectionClear();
			if ( paintMethod=="copy" ) selectionCopy();
//			if ( paintMethod=="paste" ) clipboardPaste();
			if ( paintMethod=="makeTile" ) selectionMakeTile();
		} else {
		grid.clearRect(0,0,mapWidth*charWidth,mapHeight*charHeight);
		drawScreenGrid();
		}
	}
}

//

function mouseEventMap(res,e) {
	event=e.originalEvent;
	var mx=event.layerX,
		my=event.layerY,
		cx=Math.floor(mx/charWidth),
		cy=Math.floor(my/charHeight);
		
	if (event.button==0) { // events for left button mouse
		 if (res == 'down') {
			flag = true;
			dot_flag = true;
			if (dot_flag) {
				if (drawMode=="paint") doDrawOnMap(cx,cy);
				if (drawMode=="select") mouseDown_Select(event);
				if (drawMode=="object") mouseDown_Object(event);
				dot_flag = false;
			}
		}
		if (res == 'up' || res == "out") {
			flag = false;
			if (drawMode=="paint") {
				if (res == "out") {
					grid.lineWidth=1; grid.strokeStyle='#000000';
					draw_Cursor(ocx,ocy); // clear old cursor
					drawScreenGrid();
				}
				if (res == 'up') doDrawOnMap(cx,cy);
			}
			if (drawMode=="selecting" && res=="up") mouseUp_Select(event);
			if (res=="up" && drawMode=="paste") {
				doClipboardPaste(cx,cy);
			}
		}
		if (res == 'move') {
			if ( (cx!==ocx) || (cy!==ocy) ) {
				if (drawMode=="paint" || drawMode=="select" || drawMode=="paste") {
					grid.lineWidth=1; grid.strokeStyle='#000000';
					draw_Cursor(ocx,ocy); // clear old cursor
					drawScreenGrid();

					if (drawMode=="paint") grid.strokeStyle='#ffffff';
					if (drawMode=="select" || drawMode=="paste") grid.strokeStyle='blue';
					grid.lineWidth=1;
					draw_Cursor(cx,cy);
				}
				
				if (drawMode=="selecting") mouseMove_Select(event);
				ocx=cx; ocy=cy;
			}
			if (flag) {
				if ( drawMode=="paint" ) doDrawOnMap(cx,cy);
			}
		}
	}
}

var selectTimer;

function animMapSelect() {
	if ( grid===undefined ) return;
	grid.strokeStyle='#000000';	grid.setLineDash([]);
	draw_Selection();

	if (drawMode!="select" && drawMode!="paste" && drawMode!="object" || selectCancel) {
		grid.clearRect(0,0,mapWidth*charWidth,mapHeight*charHeight);
		drawScreenGrid();
		selectCancel=false;
		clearTimeout(selectTimer);
		return;
	}
	selectOffset++;
	if (selectOffset>24) selectOffset=0;

	if ( drawMode=="select" || drawMode=="paste" ) grid.strokeStyle='blue';
	if ( drawMode=="object") grid.strokeStyle='yellow';
	
	grid.setLineDash([4,8]);
	grid.lineDashOffset=-selectOffset; grid.lineWidth=1;
	draw_Selection();
	grid.setLineDash([]); grid.lineDashOffset=0;

	selectTimer=setTimeout(animMapSelect, 20);			
}

//
//
//

function animMap() {
	if (currentChars==0) {
		for(adr=0;adr<dataMap.length;adr++) {
			ch=dataMap[adr];
			if (ch>=32 && ch<=35) {
				ch++; if (ch>35) ch=32;
				dataMap[adr]=ch;
				y=Math.floor(adr/mapWidth);
				x=adr % mapWidth;
				drawChar(x,y,ch,map);
			}
		}
	}
	setTimeout(animMap,100);
}
//
//
//
function initMapEditor() {
//	createNewLocation('My first location',64,16);
	animMap();
}
